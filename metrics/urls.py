from django.conf.urls import patterns, url

urlpatterns = patterns('',
    url(r'^metrics/$', 'metrics.views.home'),
    url(r'^metrics/all/new/$', 'metrics.views.get_all_new'),
    url(r'^metrics/all/total/$', 'metrics.views.get_all_total'),
    url(r'^metrics/joblistings/industry/$', 'metrics.views.get_ind_jobs'),
    url(r'^metrics/jobseekers/industry/$', 'metrics.views.get_ind_seks'),
)
