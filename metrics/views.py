from django.shortcuts import render
from django.http import HttpResponse
from common.models import Jobseeker, Employer, JobListing, Application, Industry
from datetime import datetime, timedelta
import json

def home(request): return render(request, 'graphs.html', {'employers': Employer.objects.all()})

def get_new_items(manager):
    items = manager.all().order_by('date_created')
    if len(items) == 0: return []
    first_date = items[0].date_created - timedelta(days=items[0].date_created.weekday())
    date = first_date
    rets = []
    while date <= datetime.now().date():
        rets.append({'date': date.strftime("%B %d"), 'items': 0 })
        date += timedelta(days=7)
    for item in items:
        rets[int((item.date_created - first_date).days / 7)]['items'] += 1
    return rets

def get_total_items(manager):
    rets = get_new_items(manager)
    for i in range(1,len(rets)):
        rets[i]['items'] += rets[i-1]['items']
    return rets

def get_new_employers(request): return render(request, json.dumps(get_new_items(Employer.objects)), content_type='application/json')
def get_total_employers(request): return render(request, json.dumps(get_total_items(Employer.objects)), content_type='application/json')
def get_new_jobseekers(request): return render(request, json.dumps(get_new_items(Jobseeker.objects)), content_type='application/json')
def get_total_jobseekers(request): return render(request, json.dumps(get_total_items(Jobseeker.objects)), content_type='application/json')
def get_new_listings(request): return render(request, json.dumps(get_new_items(JobListing.objects)), content_type='application/json')
def get_total_listings(request): return render(request, json.dumps(get_total_items(JobListing.objects)), content_type='application/json')
def get_new_applications(request): return render(request, json.dumps(get_new_items(Application.objects)), content_type='application/json')
def get_total_applications(request): return render(request, json.dumps(get_total_items(Application.objects)), content_type='application/json')

def convert_all(request, f):
    emps = f(Employer.objects)
    seks = f(Jobseeker.objects)
    lsts = f(JobListing.objects)
    apps = f(Application.objects)
    f_e = f_s = f_l = f_a = datetime.now().date() + timedelta(days=7)
    if len(emps) > 0: f_e = datetime.strptime(emps[0]['date'], '%B %d').date()
    if len(seks) > 0: f_s = datetime.strptime(seks[0]['date'], '%B %d').date()
    if len(lsts) > 0: f_l = datetime.strptime(lsts[0]['date'], '%B %d').date()
    if len(apps) > 0: f_a = datetime.strptime(apps[0]['date'], '%B %d').date()
    min_date = min(min(min(f_e,f_s),f_l),f_a)
    d_e = int((f_e - min_date).days / 7)
    d_s = int((f_s - min_date).days / 7)
    d_l = int((f_l - min_date).days / 7)
    d_a = int((f_a - min_date).days / 7)
    for i in range(0,d_e):
        emps = [{ 'date': str(min_date + timedelta(days=i*7)), 'items': 0 }] + emps
    for i in range(0,d_s):
        seks = [{ 'date': str(min_date + timedelta(days=i*7)), 'items': 0 }] + seks
    for i in range(0,d_l):
        lsts = [{ 'date': str(min_date + timedelta(days=i*7)), 'items': 0 }] + lsts
    for i in range(0,d_a):
        apps = [{ 'date': str(min_date + timedelta(days=i*7)), 'items': 0 }] + apps
    rets = []
    for i in range(0,len(emps)): # all should now have same length
        rets.append([emps[i]['date'], emps[i]['items'], seks[i]['items'], lsts[i]['items'], apps[i]['items']])
    return HttpResponse(json.dumps(rets), content_type='application/json')

def get_all_new(request): return convert_all(request, get_new_items)
def get_all_total(request): return convert_all(request, get_total_items)


def get_industry_count(obj):
    industries = Industry.objects.all()
    key = []
    ret = []
    for industry in industries:
        key.append(industry.name)
        try:
            ret.append(len(obj.objects.filter(industries__in=[industry])))
        except:
            ret.append(len(obj.objects.filter(industry=industry)))
    return HttpResponse(json.dumps([key,ret]), content_type='application/json')

def get_ind_jobs(request): return get_industry_count(JobListing)
def get_ind_seks(request): return get_industry_count(Jobseeker)


    
