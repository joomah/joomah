# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0006_employer_auth'),
    ]

    operations = [
        migrations.AddField(
            model_name='language',
            name='country',
            field=models.ForeignKey(blank=True, to='common.Location', null=True),
            preserve_default=True,
        ),
    ]
