# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0008_auto_20150608_1505'),
    ]

    operations = [
        migrations.AlterField(
            model_name='employer',
            name='primary_address',
            field=models.CharField(default='Hidden', max_length=255),
            preserve_default=False,
        ),
    ]
