# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

def convertEducationEventLevel(apps, schema_editor):
    EducationEvent = apps.get_model('common','educationevent')
    db_alias = schema_editor.connection.alias
    EducationEvent.objects.using(db_alias).filter(level__gt=1).update(level=models.F('level')+4)

class Migration(migrations.Migration):

    dependencies = [
        ('common', '0003_auto_20150326_1306'),
    ]

    operations = [
        migrations.RunPython(
            convertEducationEventLevel,
        ),
        migrations.AddField(
            model_name='educationevent',
            name='activities',
            field=models.CharField(max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='educationevent',
            name='honors',
            field=models.CharField(max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='educationevent',
            name='location',
            field=models.CharField(max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='jobseeker',
            name='nationality',
            field=models.CharField(blank=True, max_length=100, null=True, choices=[(b'Ghana', b'Ghana'), (b'Congo', b'Congo'), (b'Nigeria', b'Nigeria')]),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='jobseeker',
            name='street_address',
            field=models.CharField(max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='educationevent',
            name='level',
            field=models.PositiveIntegerField(default=0, choices=[(0, b'Degree'), (0, b'None'), (1, b'High School'), (2, b"'A' Level"), (3, b'HND'), (4, b'GCSE'), (5, b"Teacher's Certificate"), (6, b'Associates degree'), (7, b'Bachelors degree'), (8, b'Honours degree'), (9, b'Masters degree'), (10, b'Doctorate degree'), (11, b'Professional degree')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='joblisting',
            name='min_degree',
            field=models.PositiveIntegerField(default=0, choices=[(0, b'Degree'), (0, b'None'), (1, b'High School'), (2, b"'A' Level"), (3, b'HND'), (4, b'GCSE'), (5, b"Teacher's Certificate"), (6, b'Associates degree'), (7, b'Bachelors degree'), (8, b'Honours degree'), (9, b'Masters degree'), (10, b'Doctorate degree'), (11, b'Professional degree')]),
            preserve_default=True,
        ),
    ]
