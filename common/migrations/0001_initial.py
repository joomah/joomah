# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
import common.validators
import django.utils.timezone
from django.conf import settings
import common.models
import phonenumber_field.modelfields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Jobseeker',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(default=django.utils.timezone.now, verbose_name='last login')),
                ('profile_photo', models.ImageField(default=b'placeholder.png', upload_to=common.models.upload_to_photos)),
                ('date_of_birth', models.DateField(null=True, blank=True)),
                ('gender', models.CharField(blank=True, max_length=10, choices=[(b'M', b'Male'), (b'F', b'Female'), (b'N', b'Other')])),
                ('abstract', models.TextField(null=True, blank=True)),
                ('verified', models.BooleanField(default=False)),
                ('email', models.EmailField(unique=True, max_length=50)),
                ('first_name', models.CharField(max_length=50)),
                ('last_name', models.CharField(max_length=50)),
                ('phone_number', phonenumber_field.modelfields.PhoneNumberField(max_length=128, null=True, blank=True)),
                ('resume', models.FileField(blank=True, null=True, upload_to=common.models.upload_to_documents, validators=[common.validators.validate_document])),
                ('date_created', models.DateField(auto_now_add=True)),
                ('enneagram_results', models.PositiveIntegerField(null=True, blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Employer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(default=django.utils.timezone.now, verbose_name='last login')),
                ('logo', models.ImageField(default=b'placeholder.png', upload_to=common.models.upload_to_photos)),
                ('header_color', models.CharField(max_length=8)),
                ('name', models.CharField(unique=True, max_length=50)),
                ('description', models.TextField(null=True, blank=True)),
                ('primary_address', models.CharField(max_length=255, null=True, blank=True)),
                ('email', models.EmailField(max_length=50)),
                ('phone_number', models.CharField(max_length=20, null=True, blank=True)),
                ('year_founded', models.PositiveIntegerField(null=True, blank=True)),
                ('number_of_employees', models.PositiveIntegerField(null=True, blank=True)),
                ('hire_in_year', models.CharField(blank=True, max_length=64, null=True, choices=[(b'Less than 5 times', b'Less than 5 times'), (b'5 to 10 times', b'5 to 10 times'), (b'10 to 15 times', b'10 to 15 times'), (b'15 to 30 times', b'15 to 30 times'), (b'Greater than 30 times', b'Greater than 30 times')])),
                ('interns', models.BooleanField(default=False)),
                ('interns_in_year', models.CharField(blank=True, max_length=64, null=True, choices=[(b'Less than 5 times', b'Less than 5 times'), (b'5 to 10 times', b'5 to 10 times'), (b'10 to 30 times', b'10 to 30 times'), (b'Greater than 30 times', b'Greater than 30 times')])),
                ('date_created', models.DateField(auto_now_add=True)),
                ('next_mail_date', models.DateTimeField(null=True, blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Application',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cover_letter', models.TextField(null=True, blank=True)),
                ('doc1', models.FileField(blank=True, null=True, upload_to=common.models.upload_to_documents, validators=[common.validators.validate_document])),
                ('doc2', models.FileField(blank=True, null=True, upload_to=common.models.upload_to_documents, validators=[common.validators.validate_document])),
                ('rating', models.PositiveIntegerField(blank=True, null=True, choices=[(0, b'Terrible'), (1, b'Bad'), (2, b'Okay'), (3, b'Good'), (4, b'Great'), (5, b'Must-Hire')])),
                ('forced', models.BooleanField(default=False)),
                ('date_created', models.DateField(auto_now_add=True)),
                ('can_interview', models.BooleanField(default=False)),
                ('interview_time', models.DateTimeField(null=True, blank=True)),
                ('applicant', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='BlogEntry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=127)),
                ('content', models.TextField()),
                ('key_in_wp', models.PositiveIntegerField()),
            ],
            options={
                'ordering': ['-id'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CareerEvent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('employer_name', models.CharField(max_length=50, verbose_name='Employer')),
                ('job_title', models.CharField(max_length=50)),
                ('job_description', models.TextField(null=True, blank=True)),
                ('start_date', models.DateField()),
                ('end_date', models.DateField(null=True, blank=True)),
                ('salary', models.PositiveIntegerField(blank=True, null=True, choices=[(b'', b'Undisclosed'), (0, b'Unpaid'), (1, b'< 10,000 Cedis'), (2, b'10,000-20,000 Cedis'), (3, b'20,000-30,000 Cedis'), (4, b'30,000-60,000 Cedis'), (5, b'60,000-90,000 Cedis'), (6, b'90,000-120,000 Cedis'), (7, b'120,000-250,000 Cedis'), (8, b'> 250,000 Cedis')])),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='EducationEvent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('institution_name', models.CharField(max_length=50, verbose_name='Institution')),
                ('level', models.PositiveIntegerField(default=0, choices=[(0, b'Degree'), (0, b'None'), (1, b'High School'), (2, b'Associates degree'), (3, b'Bachelors degree'), (4, b'Honours degree'), (5, b'Masters degree'), (6, b'Doctorate degree'), (7, b'Professional degree')])),
                ('qualification', models.CharField(max_length=50, null=True, blank=True)),
                ('start_date', models.PositiveIntegerField()),
                ('end_date', models.PositiveIntegerField(null=True, blank=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'education event',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ExtensionChange',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('jobseeker', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='HardClonedSeeker',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('seeker', models.TextField()),
                ('jobs', models.TextField()),
                ('schools', models.TextField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Industry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='JobListing',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=50)),
                ('body', models.TextField()),
                ('start_date', models.DateField(auto_now_add=True)),
                ('date_created', models.DateField(auto_now_add=True)),
                ('end_date', models.DateField()),
                ('otherRequirements', models.TextField(null=True, blank=True)),
                ('experience', models.PositiveIntegerField(default=0, choices=[(0, b'Entry Level'), (1, b'1-3 years'), (3, b'3-5 years'), (5, b'5-10 years'), (10, b'10+ years')])),
                ('min_degree', models.PositiveIntegerField(default=0, choices=[(0, b'Degree'), (0, b'None'), (1, b'High School'), (2, b'Associates degree'), (3, b'Bachelors degree'), (4, b'Honours degree'), (5, b'Masters degree'), (6, b'Doctorate degree'), (7, b'Professional degree')])),
                ('date_posted', models.DateTimeField(auto_now_add=True)),
                ('salary', models.PositiveIntegerField(blank=True, null=True, choices=[(b'', b'Undisclosed'), (0, b'Unpaid'), (1, b'< 10,000 Cedis'), (2, b'10,000-20,000 Cedis'), (3, b'20,000-30,000 Cedis'), (4, b'30,000-60,000 Cedis'), (5, b'60,000-90,000 Cedis'), (6, b'90,000-120,000 Cedis'), (7, b'120,000-250,000 Cedis'), (8, b'> 250,000 Cedis')])),
                ('random_ranking', models.BooleanField(default=False)),
                ('calendar', models.TextField(default=b'[]')),
                ('commitment', models.PositiveIntegerField(default=0, choices=[(0, b'Full Time'), (1, b'Part Time'), (2, b'Contract'), (3, b'Internship')])),
                ('number_documents_required', models.PositiveIntegerField(default=0, choices=[(0, b'0'), (1, b'1'), (2, b'2')])),
                ('views', models.PositiveIntegerField(default=0)),
                ('industry', models.ForeignKey(to='common.Industry')),
            ],
            options={
                'ordering': ['-date_posted'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Jobseeker_auth',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('auth_code', models.CharField(unique=True, max_length=30)),
                ('creation_date', models.DateTimeField(default=datetime.datetime.now)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Language',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Location',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('city', models.CharField(max_length=50, null=True, blank=True)),
                ('country', models.CharField(max_length=50, choices=[(b'Ghana', b'Ghana'), (b'Congo', b'Congo'), (b'Nigeria', b'Nigeria')])),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Reference',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('address', models.CharField(max_length=255, null=True, blank=True)),
                ('email', models.EmailField(max_length=75, null=True, blank=True)),
                ('phone_number', models.CharField(max_length=255, null=True, blank=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SavedSearch',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('query', models.TextField()),
                ('date_created', models.DateField(auto_now_add=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Skill',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=127)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='joblisting',
            name='languages',
            field=models.ManyToManyField(to='common.Language'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='joblisting',
            name='location',
            field=models.ForeignKey(to='common.Location'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='joblisting',
            name='owner',
            field=models.ForeignKey(to='common.Employer'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='joblisting',
            name='requirements',
            field=models.ManyToManyField(to='common.Skill', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='employer',
            name='industries',
            field=models.ManyToManyField(to='common.Industry'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='employer',
            name='locations',
            field=models.ManyToManyField(to='common.Location'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='careerevent',
            name='industry',
            field=models.ForeignKey(to='common.Industry'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='careerevent',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='application',
            name='job',
            field=models.ForeignKey(to='common.JobListing'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='application',
            unique_together=set([('applicant', 'job')]),
        ),
        migrations.AddField(
            model_name='jobseeker',
            name='connections',
            field=models.ManyToManyField(to='common.Employer'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='jobseeker',
            name='industries',
            field=models.ManyToManyField(to='common.Industry', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='jobseeker',
            name='languages',
            field=models.ManyToManyField(to='common.Language', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='jobseeker',
            name='location',
            field=models.ForeignKey(blank=True, to='common.Location', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='jobseeker',
            name='skills',
            field=models.ManyToManyField(to='common.Skill', null=True, blank=True),
            preserve_default=True,
        ),
    ]
