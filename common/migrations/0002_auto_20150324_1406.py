# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='employer',
            name='prefered_languages',
            field=models.ManyToManyField(to='common.Language', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='employer',
            name='prefered_skills',
            field=models.ManyToManyField(to='common.Skill', null=True, blank=True),
            preserve_default=True,
        ),
    ]
