# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0002_auto_20150324_1406'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='joblisting',
            name='number_documents_required',
        ),
        migrations.AlterField(
            model_name='application',
            name='applicant',
            field=models.ForeignKey(to='common.Jobseeker'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='careerevent',
            name='user',
            field=models.ForeignKey(to='common.Jobseeker'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='educationevent',
            name='user',
            field=models.ForeignKey(to='common.Jobseeker'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='extensionchange',
            name='jobseeker',
            field=models.ForeignKey(to='common.Jobseeker'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='jobseeker_auth',
            name='user',
            field=models.ForeignKey(to='common.Jobseeker'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='reference',
            name='user',
            field=models.ForeignKey(to='common.Jobseeker'),
            preserve_default=True,
        ),
    ]
