# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0012_auto_20150811_1622'),
    ]

    operations = [
        migrations.AddField(
            model_name='employer',
            name='category',
            field=models.CharField(default=b'Direct Employer', max_length=100),
            preserve_default=True,
        ),
    ]
