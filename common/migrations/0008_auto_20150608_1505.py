# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0007_language_country'),
    ]

    operations = [
        migrations.AlterField(
            model_name='employer_auth',
            name='user',
            field=models.ForeignKey(to='common.Employer'),
            preserve_default=True,
        ),
    ]
