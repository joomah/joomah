from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    url(r'^authenticate/$', 'common.views.authenticate'),
    url(r'^logout/$', 'common.views.logout'),    
    url(r'^mobile/logout/$', 'common.views.mobile_logout'),    
    url(r'^deregister/$', 'common.views.deregister')
)
