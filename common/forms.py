from django import forms
from django.db import models
from django.forms import ModelForm
from common.models import Document

class DocumentForm(ModelForm):
    class Meta:
        model = Document
        exclude = ('upload_date',)
