from django.template import Library

register = Library()

'''
@register.filter
def get_industries(unused=0):
    return [ "Agriculture",
        "Accounting",
        "Advertising",
        "Aerospace",
        "Aircraft",
        "Airline",
        "Apparel & Accessories",
        "Automotive",
        "Banking",
        "Broadcasting",
        "Brokerage",
        "Biotechnology",
        "Call Centers",
        "Cargo Handling",
        "Chemical",
        "Computer",
        "Consulting",
        "Consumer Products",
        "Cosmetics",
        "Defense",
        "Department Stores",
        "Education",
        "Electronics",
        "Energy",
        "Entertainment & Leisure",
        "Executive Search",
        "Financial Services",
        "Food, Beverage & Tobacco",
        "Grocery",
        "Health Care",
        "Internet Publishing",
        "Investment Banking",
        "Legal",
        "Manufacturing",
        "Motion Picture & Video",
        "Music",
        "Newspaper Publishers",
        "Online Auctions",
        "Other",
        "Pension Funds",
        "Pharmaceuticals",
        "Private Equity",
        "Publishing",
        "Real Estate",
        "Retail & Wholesale",
        "Securities & Commodity Exchanges",
        "Service",
        "Soap and Detergent",
        "Software",
        "Sports",
        "Technology",
        "Telecommunications",
        "Television",
        "Transportaion",
        "Venture Capital" ]

@register.filter
def get_countries(unused):
    return [ "Ghana", "Kenya" ]
'''
