from django.db import models
from django.db.models import Q
from django.db.models import F
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.db.models.signals import post_save
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError
from django.db.models.signals import pre_save
from django.core.mail import send_mail
from django.core.mail import EmailMultiAlternatives
from django.utils.timezone import utc
from common.validators import validate_document
from datetime import datetime, timedelta
from phonenumber_field.modelfields import PhoneNumberField
import jobseeker.settings
from twilio.rest import TwilioRestClient
import string
import random
from django.forms.models import model_to_dict
import json
import re, os
#from __future__ import unicode_literals

def upload_to_photos(instance, filename):
    return '%s/photos/%s' % (instance.id, filename)
    
def upload_to_documents(instance, filename):
    return '%s/documents/%s' % (instance.id, filename)

class SavedSearch(models.Model):
    query = models.TextField()
    date_created = models.DateField(auto_now_add=True)

class HardClonedSeeker(models.Model):
    seeker = models.TextField()
    jobs = models.TextField()
    schools = models.TextField()
    def set_seeker(self, seeker):
        seeker_dict = model_to_dict(seeker)
        seeker_dict['date_of_birth'] = str(seeker.date_of_birth)
        seeker_dict['date_created'] = str(seeker.date_created)
        seeker_dict['phone_number'] = str(seeker.phone_number)
        seeker_dict['password'] = None
        seeker_dict['last_login'] = str(seeker.last_login)
        seeker_dict['resume'] = seeker.resume.url if seeker.resume else None
        seeker_dict['profile_photo'] = seeker.profile_photo.url if seeker.profile_photo else None
        self.seeker = json.dumps(seeker_dict)
        schools = []
        for school in seeker.educationevent_set.all():
            s = model_to_dict(school)
            s['user'] = None
            schools.append(s)
        self.schools = json.dumps(schools)
        jobs = []
        for job in seeker.careerevent_set.all():
            j = model_to_dict(job)
            j['start_date'] = str(job.start_date)
            if job.end_date: j['end_date'] = str(job.end_date)
            jobs.append(j)
        self.jobs = json.dumps(jobs)
        self.save()


class BlogEntry(models.Model):
    title = models.CharField(max_length=127)
    content = models.TextField()
    key_in_wp = models.PositiveIntegerField()
    class Meta:
        ordering = ['-id']

class Industry(models.Model):
    name = models.CharField(max_length=255)
    def __str__(self):
        return self.name
    def __unicode__(self):
        return self.name

class Language(models.Model):
    name = models.CharField(max_length=255)
    country = models.ForeignKey('Location', null=True, blank=True)
    def __str__(self):
        return self.name
    def __unicode__(self):
        return self.name

class Location(models.Model):
    COUNTRIES = (
        ('Ghana', 'Ghana'),
        ('Congo', 'Congo'),
        ('Kenya', 'Kenya'),
        ('Uganda', 'Uganda'),
        ('Nigeria', 'Nigeria'),
    )
    city = models.CharField(max_length=50,blank=True,null=True)
    country = models.CharField(max_length=50,choices=COUNTRIES)
    def __unicode__(self):
        loc = self.country
        if self.city:
            loc = self.city + ', ' + loc
        return loc
    def __str__(self):
        loc = self.country
        if self.city:
            loc = self.city + ', ' + loc
        return loc

class EmployerManager(BaseUserManager):
    def create_user(self, name, email, pwd=None):
        emp = Employer(name=name, email=email)
        if pwd: emp.set_password(pwd)
        emp.save()
        return emp

    def create_superuser(self, name, email, pwd):
        user = self.create_user(name,email,pwd=pwd)
        user.is_staff = True
        user.save()
        return user

    def get_by_natural_key(self, name):
        return self.get(name=name)


class Employer(AbstractBaseUser):
    HIRETIMES = (
        ('Less than 5 times','Less than 5 times'),
        ('5 to 10 times','5 to 10 times'),
        ('10 to 15 times','10 to 15 times'),
        ('15 to 30 times','15 to 30 times'),
        ('Greater than 30 times','Greater than 30 times'),
    )
    INTERNHIRETIMES = (
        ('Less than 5 times','Less than 5 times'),
        ('5 to 10 times','5 to 10 times'),
        ('10 to 30 times','10 to 30 times'),
        ('Greater than 30 times','Greater than 30 times'),
    )
    CATEGORY = (
        ('Direct Employer', 'Direct Employer'),
        ('Referer', 'Referer'),
        ('NGO', 'NGO'),
        ('Recruitment Agency', 'Recruitment Agency'),
    )
    SUBSCRIPTIONS = (
        ('free', 'free'),
        ('basic', 'basic'),
    )

    logo = models.ImageField(upload_to=upload_to_photos, default='placeholder.png')
    header_color = models.CharField(max_length=8)
    name = models.CharField(max_length=50,unique=True)
    description = models.TextField(blank=True, null=True)
    primary_address = models.CharField(max_length=255)
    locations = models.ManyToManyField(Location) #experiment with setting blank=True to force registration
    email = models.EmailField(max_length=50)
    phone_number = models.CharField(blank=True, null=True, max_length=20)
    year_founded = models.PositiveIntegerField(blank=True, null=True)
    # tax_id = models.CharField(max_length=63, unique=True)
    industries = models.ManyToManyField(Industry) #experiment with setting blank=True to force registration
    number_of_employees = models.PositiveIntegerField(blank=True, null=True)
    hire_in_year = models.CharField(choices=HIRETIMES, blank=True, null=True, max_length=64)
    prefered_skills = models.ManyToManyField('Skill', blank=True, null=True)
    prefered_languages = models.ManyToManyField(Language, null=True, blank=True)
    interns = models.BooleanField(default = False, blank=True)
    interns_in_year = models.CharField(choices=INTERNHIRETIMES, null=True, blank=True, max_length=64)
    date_created = models.DateField(auto_now_add=True)
    next_mail_date = models.DateTimeField(blank=True, null=True)
    category = models.CharField(max_length=100, default='Direct Employer', choices=CATEGORY)
    is_staff = models.BooleanField(default=False)
    stripe_customer_id = models.CharField(max_length=255, null=True, blank=True)
    subscription_type = models.CharField(max_length=255, default='free', choices=SUBSCRIPTIONS)
    subscription_start = models.DateTimeField(null=True, blank=True)
    subscription_end = models.DateTimeField(null=True, blank=True)
    money_in_account = models.PositiveIntegerField(default=0)
    verified = models.BooleanField(default=False)
    USERNAME_FIELD = 'name'
    objects = EmployerManager()
    def get_full_name(self):
        return self.name
    def get_short_name(self):
        return self.name
    def __str__(self):
        return self.name
    def __unicode__(self):
        return self.name

    @property
    def is_superuser(self):
        return self.is_staff

    def has_perm(self, perm, obj=None):
        return self.is_staff

    def has_module_perms(self, app_label):
        return self.is_staff

    def generate_token(self):
        ja = Employer_auth(user=self, auth_code=''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(20)))
        ja.save()
        return ja

class JobseekerManager(BaseUserManager):
    def create_user(self, email, first_name, last_name, pwd=None):
        js = Jobseeker(email=email, first_name=first_name, last_name=last_name)
        if pwd: js.set_password(pwd)
        js.save()
        return js

    def create_superuser(self, email, first_name, last_name, pwd):
        return self.create_user(email, first_name, last_name, pwd=pwd)

    def get_by_natural_key(self, email):
        return self.get(email=email)

class Jobseeker(AbstractBaseUser):
    GENDERS = (
        ('M', 'Male'),
        ('F', 'Female'),
        ('N', 'Other'),
    )


    profile_photo = models.ImageField(upload_to=upload_to_photos, default='placeholder.png')
    date_of_birth = models.DateField(null=True, blank=True)
    gender = models.CharField(max_length=10, choices=GENDERS, blank=True)
    location = models.ForeignKey(Location, null=True, blank=True)
    street_address = models.CharField(max_length=255, null=True, blank=True)
    #nationality = models.CharField(choices=Location.COUNTRIES, null=True, blank=True, max_length=100)
    abstract = models.TextField(null=True,blank=True)
    industries = models.ManyToManyField(Industry, null=True, blank=True)
    skills = models.ManyToManyField('Skill', null=True, blank=True)
    languages = models.ManyToManyField(Language, null=True, blank=True)
    verified = models.BooleanField(default=False)
    email = models.EmailField(max_length=50, unique=True)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    connections = models.ManyToManyField(Employer)
    phone_number = PhoneNumberField(blank=True, null=True)
    resume = models.FileField(upload_to=upload_to_documents, blank=True, null=True, validators=[validate_document])
    date_created = models.DateField(auto_now_add=True)
    enneagram_results = models.PositiveIntegerField(null=True, blank=True)
    USERNAME_FIELD = 'email'
    objects = JobseekerManager()
    def __init__(self, *args, **kwargs):
        super(Jobseeker, self).__init__(*args, **kwargs)
        self.__original_email = self.email
        self.__original_password = self.password
        self.__original_phone = self.phone_number == None or str(self.phone_number) == ''
    def save(self, *args, **kwargs):
        initial_pwd = kwargs.pop('initial_pwd', False)
        super(Jobseeker, self).save(*args, **kwargs)
        if self.email != self.__original_email:
            self.__original_email = self.email
            self.verified = False
        if self.password != self.__original_password:
            self.__original_password = self.password
            if not initial_pwd:
                message= (  "This message is to notify you that your JooMah account password was recently changed."
                            "\nIf you have any questions, feel free to contact us at info@joomah.com."
                            "\nThanks,\nThe JooMah Team." )
                html_message = ("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>\r\n"
                                "<html xmlns='http://www.w3.org/1999/xhtml'>\r\n"
                                "<head>\r\n"
                                "<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />\r\n"
                                "<meta name='description' content=\"J&oacute;&ograve;Mah, the place to find jobs near you. Join Top Jobs in Ghana - Ghana's Leading Job Site.\">"
                                "<title>J&oacute;&ograve;Mah: Applicant Screening Made Easy</title>"
                                "<link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>\r\n"
                                "</head>\r\n"
                                "<body style='background: #ECF0F1; font-size: 12px;'>\r\n"
                                "<div style='width: 60%; min-width: 400px; max-width: 800px; margin-left: auto; margin-right: auto; color: gray; margin-top: 40px;'>\r\n"
                                "  <div>\r\n"
                                "    <div style='color: #0096C9; background: #FFFFFF; font-size: 3em; padding: 10px;'>J&oacute;&ograve;mah</div>\r\n"
                                "    <div style='background: #0096C9; color: #FFFFFF; font-size: 2em; padding: 10px;'>Password changed</div>\r\n"
                                "  </div>\r\n"
                                "  <div style='background: #FFFFFF; margin: 30px 60px; padding: 10px;'>\r\n"
                                "    <div style='color: #A5A5A5; font-size: 1.5em;'>\r\n"
                                "      This message is to notify you that your J&oacute;&ograve;Mah account password was recently changed."
                                "      If you did not make this change and believe your J&oacute;&ograve;Mah account has been compromised,"
                                "      please contact us at <a href='mailto:info@joomah.com' style='text-decoration: none; color: #2FA8D1;'>info@joomah.com</a>."
                                "    </div>\r\n"
                                "  </div>\r\n"
                                "  <div style='color: gray;width: 200px; min-width: 600px; margin-left: auto; margin-right: auto; margin-top: 100px; text-align: center;'>\r\n"
                                "    <div>\r\n"
                                "      <span style='text-decoration: none; color: inherit; margin: 10px;'>Tell you friends and colleagues about us on "
                                "      <a href='https://www.facebook.com/pages/J%C3%B3%C3%B2Mah/827539130595240' style='text-decoration: none; color: #2FA8D1;'>Facebook</a> or "
                                "      <a href='https://twitter.com/JoomahAfrica' style='text-decoration: none; color: #2FA8D1;'>Twitter</a></span>\r\n"
                                "    </div>\r\n"
                                "    <div>\r\n"
                                "      <a href='https://jobs.joomah.com/terms'  style='text-decoration: none; color: #2FA8D1; margin: 10px;'>Privacy Policy</a>\r\n"
                                "      <a href='mailto:info@joomah.com' style='text-decoration: none; color: #2FA8D1; margin: 10px;'>Contact us</a>\r\n"
                                "    </div>\r\n"
                                "  </div>\r\n"
                                "</div>\r\n"
                                "</body>\r\n"
                               "</html>\r\n")
                msg = EmailMultiAlternatives("JooMah Password Changed", message, 'info@joomah.com', [self.email])
                msg.attach_alternative(html_message, "text/html")
                msg.send()
        if self.resume and os.path.splitext(self.resume.url)[1] != '.pdf':
            ExtensionChange.objects.create(jobseeker=self).save()
        if self.phone_number and self.__original_phone:
            client = TwilioRestClient(jobseeker.settings.TWILIO_ACCOUNT_SID, jobseeker.settings.TWILIO_AUTH_TOKEN)
            msg =  ("Welcome to JooMah!  And remember, to receive your current job notices, just text 'notice' to this number.  "
                    "Then, you can apply to a job by texting in the job id number." )
            message = client.messages.create(to=str(self.phone_number), from_="+17323720828", body=msg)
            self.__original_phone = False
        return self     

    def notices(self):
        edu = self.get_max_education()
        return JobListing.objects.exclude(id__in=map(lambda x: x.job.id, list(Application.objects.filter(applicant=self)))).exclude(
            end_date__lt=datetime.utcnow().replace(tzinfo=utc)).filter(
            industry__in=self.industries.all(),min_degree__lte=(int(edu) if edu else 0)
            ,location__country=self.location.country if self.location else Location.objects.get(id=1).country)

    def get_full_name(self):
        return self.first_name + ' ' + self.last_name

    def get_short_name(self):
        return self.first_name

    def occupation(self):
        try:
            return self.careerevent_set.get(end_date=None)
        except:
            return None

    def get_last_job(self):
        try:
            last_jobs = self.jobs.exclude(end_date=None).order_by('-end_date')
            return (last_jobs[0] if len(last_jobs) > 0 else None)
        except:
            return None

    def get_max_education(self):
        best = None
        for edu_event in self.educationevent_set.all():
            if best is None or int(edu_event) > int(best):
                best = edu_event
        return best
    
    def get_total_experience(self):
        total = timedelta()
        for job in self.careerevent_set.all():
            if job.end_date:
                total += job.end_date - job.start_date
            else:
                total += datetime.utcnow().replace(tzinfo=utc).date() - job.start_date
        return int(total.days / 30)

    def get_industry_experience(self, industry):
        total = timedelta()
        for job in self.careerevent_set.filter(industry=industry):
            if job.end_date:
                total += job.end_date - job.start_date
            else:
                total += datetime.utcnow().replace(tzinfo=utc).date() - job.start_date
        return total.days / 30

    def generate_token(self):
        ja = Jobseeker_auth(user=self, auth_code=''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(20)))
        ja.save()
        return ja

    def __unicode__(self):
        return self.get_full_name()
    def __str__(self):
        return self.get_full_name()

class Jobseeker_auth(models.Model):
    user=models.ForeignKey(Jobseeker)
    auth_code=models.CharField(max_length=30, unique=True)
    creation_date = models.DateTimeField(default=datetime.now)

    def __str__(self):
        return self.user.email + ":" + self.auth_code
    def __unicode__(self):
        return self.user.email + ":" + self.auth_code
        
class Employer_auth(models.Model):
    user=models.ForeignKey(Employer)
    auth_code=models.CharField(max_length=30, unique=True)
    creation_date = models.DateTimeField(default=datetime.now)

    def __str__(self):
        return self.user.email + ":" + self.auth_code
    def __unicode__(self):
        return self.user.email + ":" + self.auth_code

class CareerEvent(models.Model):
    employer_name = models.CharField( _('Employer'), max_length=50)
    job_title = models.CharField(max_length=50)
    job_description = models.TextField(null=True, blank=True)
    industry = models.ForeignKey(Industry)
    user = models.ForeignKey(Jobseeker)
    start_date = models.DateField()
    end_date = models.DateField(null=True, blank=True)
    def __str__(self):
        return '%s - %s' % (self.job_title, self.employer_name)
    def __unicode__(self):
        return '%s - %s' % (self.job_title, self.employer_name)

class Reference(models.Model):
    name = models.CharField(max_length=255)
    address = models.CharField(null=True, blank=True, max_length=255)
    email = models.EmailField(null=True, blank=True)
    phone_number = models.CharField(max_length=255, null=True, blank=True)
    user = models.ForeignKey(Jobseeker)

class EducationEvent(models.Model):
    LEVELS = (
            (0, 'Degree'),
            (0, 'None'),
            (1, 'High School'),
            (2, "'A' Level"),
            (3, 'HND'),
            (4, 'GCSE'),
            (5, "Teacher's Certificate"),
            (6, 'Associates degree'),
            (7, 'Bachelors degree'),
            (8, 'Honours degree'),
            (9, 'Masters degree'),
            (10, 'Doctorate degree'),
            (11, 'Professional degree'),
        )
    
    institution_name = models.CharField(_('Institution'), max_length=50)
    level = models.PositiveIntegerField(default=0,choices=LEVELS)
    qualification = models.CharField(max_length=50, blank=True, null=True)
    user = models.ForeignKey(Jobseeker)
    start_date = models.PositiveIntegerField()
    end_date = models.PositiveIntegerField(blank=True,null=True)
    honors = models.CharField(max_length=255, blank=True,null=True)
    location = models.CharField(max_length=255, blank=True,null=True)
    activities = models.CharField(max_length=255, blank=True, null=True)
    
    class Meta:
        verbose_name = "education event"
    
    def __str__(self):
        return '%s, %s' % (self.institution_name, self.get_level_display())
    def __unicode__(self):
        return '%s, %s' % (self.institution_name, self.get_level_display())

    def __int__(self):
        return self.level

class JobListing(models.Model):
    COMMITMENTS = (
            (0, 'Full Time'),
            (1, 'Part Time'),
            (2, 'Contract'),
            (3, 'Internship'),
        )
    EXPERIENCE = (
            (0, 'Entry Level'),
            (1, '1-3 years'),
            (3, '3-5 years'),
            (5, '5-10 years'),
            (10, '10+ years'),
        )

    title = models.CharField(max_length=50)
    body = models.TextField()
    industry = models.ForeignKey(Industry)
    start_date = models.DateField(auto_now_add=True)
    date_created = models.DateField(auto_now_add=True)
    end_date = models.DateField()
    otherRequirements = models.TextField(blank=True,null=True)
    experience = models.PositiveIntegerField(default=0, choices=EXPERIENCE)
    languages = models.ManyToManyField(Language)
    location = models.ForeignKey(Location)
    requirements = models.ManyToManyField('Skill', null=True, blank=True)
    min_degree = models.PositiveIntegerField(default=0,choices=EducationEvent.LEVELS)
    date_posted = models.DateTimeField(auto_now_add=True)
    owner = models.ForeignKey(Employer)
    random_ranking = models.BooleanField(default=False)
    calendar = models.TextField(default='[]') # JSON serialized calendar
    commitment = models.PositiveIntegerField(default=0,choices=COMMITMENTS)
    views = models.PositiveIntegerField(default=0)
    
    class Meta:
        ordering = ['-date_posted']
    
    def __str__(self):
        return self.title
    def __unicode__(self):
        return self.title
    def num_applicants(self):
        return self.application_set.count()
    
class Application(models.Model):
    RATINGS = (
        (0, 'Terrible'),
        (1, 'Bad'),
        (2, 'Okay'),
        (3, 'Good'),
        (4, 'Great'),
        (5, 'Must-Hire'),
    )
    job = models.ForeignKey(JobListing)
    applicant = models.ForeignKey(Jobseeker)
    cover_letter = models.TextField(blank=True, null=True)
    doc1 = models.FileField(upload_to=upload_to_documents, blank=True, null=True, validators=[validate_document])
    doc2 = models.FileField(upload_to=upload_to_documents, blank=True, null=True, validators=[validate_document])
    rating = models.PositiveIntegerField(choices=RATINGS, blank=True, null=True)
    forced = models.BooleanField(default=False)
    date_created = models.DateField(auto_now_add=True)
    can_interview = models.BooleanField(default=False)
    interview_time = models.DateTimeField(blank=True,null=True)
    shortlist = models.BooleanField(default=False)
    reject = models.BooleanField(default=False)

    class Meta:
        unique_together = ('applicant','job')


class Skill(models.Model):
    name = models.CharField(max_length=127)
    def __unicode__(self):
        return self.name
    def __str__(self):
        return self.name

class ExtensionChange(models.Model):
    jobseeker = models.ForeignKey(Jobseeker)
