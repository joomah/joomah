from django.http import HttpResponseRedirect
from django.contrib import auth
from django.contrib.auth.decorators import login_required


def authenticate(request):
    username = request.POST.get('username', '')
    password = request.POST.get('password', '')
    # Get user with given username/password combination
    user = auth.authenticate(username=username, password=password)

    redirect_to = request.POST.get('start',request.GET.get('start',''))

    if user is not None:
        # Log in user if active
        if user.is_active:
            auth.login(request, user)
            if not request.POST.get('remember_me',False):
                request.session.set_expiry(0)
            return HttpResponseRedirect(redirect_to)
    if request.POST.get('mobile',False):
        return HttpResponseRedirect('/mobile/login/?start=' + redirect_to  + '&retries=' + str(1 + int(request.POST.get('retries','0'))))
    return HttpResponseRedirect('/login/?start=' + redirect_to + '&retries=' + str(1 + int(request.POST.get('retries','0'))))

@login_required
def logout(request):
    auth.logout(request)
    if 'mobile' in request.META.get('HTTP_REFERER',[]):
        return HttpResponseRedirect('/mobile/login/')
    return HttpResponseRedirect('/login/')

@login_required
def mobile_logout(request):
    auth.logout(request)
    return HttpResponseRedirect('/mobile/login/')

@login_required
def deregister(request):
    user = request.user
    auth.logout(request)
    user.delete()
    return HttpResponseRedirect('/')
