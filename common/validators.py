from django.forms import ValidationError
#import magic

__all__ = ("validate_document",)

mimetypes = [ b'application/pdf', b'application/msword', b'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ]

def validate_document(value):
        try:
            mime = magic.from_buffer(value.read(), mime=True)
            print(str(mime))
            if not mime in mimetypes:
                raise ValidationError('%s is not an acceptable file type' % value)
        except AttributeError as e:
            raise ValidationError('This value could not be validated for file type' % value)