from django.shortcuts import render
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from jobseeker.forms import RegistrationForm, EditJobseekerForm, MobileEditJobseekerForm, ReferenceForm, CareerEventForm, ChangePasswordForm, EducationEventForm, MobileSearchForm, SearchForm, PersonalityQuizForm, BlogEntryForm, ApplyForm, SkillForm, ApplyFormThree
from common.models import EducationEvent, CareerEvent, JobListing, Application, Jobseeker_auth, Jobseeker, BlogEntry, SavedSearch, Reference, Location, Language
from django.db.models import Q
from django.core.mail import send_mail, EmailMultiAlternatives
from django.utils.timezone import utc
from datetime import datetime, timedelta
from django.template.loader import get_template
from django.template import Context
from io import BytesIO
from django.http import HttpResponse, HttpResponseRedirect, Http404
#from ho import pisa
import json
import re

def get(a, i, d):
    try:
        return a[i]
    except:
        return d

html_head = ("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>\r\n"
             "<html xmlns='http://www.w3.org/1999/xhtml'>\r\n"
             "<head>\r\n"
             "<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />\r\n"
             "<meta name='description' content=\"J&oacute;&ograve;Mah, the place to find jobs near you. Join Top Jobs in Ghana - Ghana's Leading Job Site.\">\r\n"
             "<title>J&oacute;&ograve;Mah: Applicant Screening Made Easy</title>\r\n"
             "<link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>\r\n"
             "</head>\r\n"
             "<body style='background: #ECF0F1; font-size: 12px;'>\r\n"
             "<div style='width: 60%; min-width: 400px; max-width: 800px; margin-left: auto; margin-right: auto; color: gray; margin-top: 40px;'>\r\n")
html_foot = ("  <div style='color: gray;width: 200px; min-width: 600px; margin-left: auto; margin-right: auto; margin-top: 100px; text-align: center;'>\r\n"
             "    <div>\r\n"
             "      <span style='text-decoration: none; color: inherit; margin: 10px;'>Tell you friends and colleagues about us on "
             "      <a href='https://www.facebook.com/pages/J%C3%B3%C3%B2Mah/827539130595240' style='text-decoration: none; color: #2FA8D1;'>Facebook</a> or "
             "      <a href='https://twitter.com/JoomahAfrica' style='text-decoration: none; color: #2FA8D1;'>Twitter</a></span>\r\n"
             "    </div>\r\n"
             "    <div>\r\n"
             "      <a href='http://jobs.joomah.com/terms' style='text-decoration: none; color: #2FA8D1; margin: 10px;'>Privacy Policy</a>\r\n"
             "      <a href='mailto:info@joomah.com' style='text-decoration: none; color: #2FA8D1; margin: 10px'>Contact us</a>\r\n"
             "    </div>\r\n"
             "  </div>\r\n"
             "</div>\r\n"
             "</body>\r\n"
             "</html>\r\n")

@login_required
def job_history(request, prefix='desktop'):
    jobs = []
    for app in Application.objects.filter(applicant=request.user).order_by('-date_created'):
        open = app.job.end_date > datetime.utcnow().replace(tzinfo=utc).date()
        jobs.append([app.job,open,app.date_created])
    return render(request, prefix + '/app_history.html', { 'jobs': jobs, 'selected': 'history' })

@login_required
def mobile_job_history(request): return job_history(request, 'mobile')

@login_required
def unsubscribe(request):
    request.user.subscribed = False
    request.user.save()
    return HttpResponse('<html><body>You have successfully unsubscribed.</body></html>')

@login_required    
def notice(request, prefix='desktop'):
    incomplete = []
    if request.GET.get('first'):
        if not request.user.skills.exists(): incomplete.append('skills')
        if not request.user.industries.exists(): incomplete.append('industries')
        if not request.user.languages.exists(): incomplete.append('languages')
        if not request.user.location: incomplete.append('location')
        if not request.user.date_of_birth: incomplete.append('date of birth')
        if not request.user.gender: incomplete.append('gender')
        if not request.user.abstract: incomplete.append('description')
    notices = []
    jobs = list(request.user.notices()) if request.user.notices else []
    return render(request, prefix + '/notice.html', { 'notices': jobs, 'selected': 'notice', 'incomplete': incomplete })

@login_required
def mobile_notice(request): return notice(request, 'mobile')

def mobile_landing(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/mobile/start/?first=1')
    jobs = JobListing.objects.order_by('-date_created')[:5]
    return render(request, '/mobile/landing_page.html', { 'jobs': jobs, 'next': request.GET.get('next',request.POST.get('next','')) })

def mobile_signin(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/mobile/start/?first=1')
    return render(request, 'mobile/signin.html', { 'next': request.POST.get('next',request.GET.get('next',''))})

@login_required
def mobile_home(request): return render(request, 'mobile/home.html', { 'next': request.POST.get('next',request.GET.get('next',''))})
def terms(request): return render(request, 'desktop/terms.html')
def mobile_terms(request): return render(request, 'mobile/terms.html')
def about_joomah(request): return render(request, 'desktop/aboutjoomah.html')

def verify(request, key):
    try:
        ja = Jobseeker_auth.objects.get(auth_code=key)
    except ObjectDoesNotExist:
        raise Http404
    ja.user.verified = True
    ja.user.save()
    user = auth.authenticate(username=ja.auth_code)
    auth.login(request, user)
    ja.delete()
    return HttpResponseRedirect('/notice/')

@login_required
def send_verification(request):
    new_token = request.user.generate_token()
    subject="JooMah Account Recovery"
    html_content = ("<div>\r\n"
                    "  <div style='color: #0096C9; background: #FFFFFF; font-size: 3em; padding: 10px;'>J&oacute;&ograve;mah</div>\r\n"
                    "  <div style='background: #0096C9; color: #FFFFFF; font-size: 2em; padding: 10px;'>Account Recovery</div>\r\n"
                    "</div>\r\n"
                    "<div style='background: #FFFFFF; margin: 30px 60px; padding: 10px;'>\r\n"
                    "  <div style='color: #A5A5A5; font-size: 1.5em;'>To verify your account please follow the link below. You will not be able to apply for a job until you verify your email address.</div>\r\n"
                    "</div>\r\n"
                    "<div style='background: #888888; width: 200px; margin-left: auto; margin-right: auto; text-align: center; padding: 3px; margin-top: 20px;'>\r\n"
                    "  <a href='http://jobs.joomah.com/verify/"+str(new_token.auth_code)+"' style='text-decoration: none; color: #FFFFFF; font-size: 3em'>Verify</a>\r\n"
                    "</div>\r\n")
    html_message = html_head + html_content + html_foot
    message="To verify your account please follow the link to http://jobs.joomah.com/verify/" + str(new_token.auth_code)
    message+="\nIf you do not verify your email address you will be unable to apply to a job."
    message+="\nThanks,"+"\n"+"The JooMah Team."
    from_email, to = 'info@joomah.com', request.user.email
    msg = EmailMultiAlternatives(subject, message, from_email, [to])
    msg.attach_alternative(html_message, "text/html")
    msg.send()
    return HttpResponseRedirect('/notice/?sent_verification=true')

def forgot_password(request):
    if request.method == 'POST':
        try:
            user = Jobseeker.objects.get(email=request.POST.get('email'))
        except ObjectDoesNotExist:
            return render(request, 'forgot_password.html', { 'errors': 'Email address is not associated with an account.' })
        new_token = user.generate_token()
        subject="JooMah Account Recovery"
        html_content = ("<div>\r\n"
                        "  <div style='color: #0096C9; background: #FFFFFF; font-size: 3em; padding: 10px;'>J&oacute;&ograve;mah</div>\r\n"
                        "    <div style='background: #0096C9; color: #FFFFFF; font-size: 2em; padding: 10px;'>Account Recovery"
                        "    </div>                               "
                        "</div>\r\n"
                        "<div style='background: #FFFFFF; margin: 30px 60px; padding: 10px;'>\r\n"
                        "  <div style='color: #A5A5A5; font-size: 1.5em;'>To recover your account please follow the link below. If you did not request account recovery you can safely ignore this message.</div>\r\n"
                        "</div>\r\n"
                        "<div style='background: #888888; width: 200px; margin-left: auto; margin-right: auto; color: #FFFFFF; text-align: center; font-size: 3em;          padding: 3px; margin-top: 20px;'>\r\n"
                        "  <a href='http://jobs.joomah.com/recover/"+ new_token.auth_code+"' style='text-decoration: none; color: inherit;'>Recover</a>\r\n"
                        "</div>\r\n")
        html_message = html_head + html_content + html_foot
        message="To recover your account please follow the link to http://jobs.joomah.com/recover/" + new_token.auth_code 
        message+="\nIf you did not request account recovery you can safely ignore this message."
        message+="\nThanks,"+"\n"+"The JooMah Team."
        from_email, to = 'info@joomah.com', request.POST.get('email')
        msg = EmailMultiAlternatives(subject, message, from_email, [to])
        msg.attach_alternative(html_message, "text/html")
        msg.send()
        return render(request, 'forgot_password.html', { 'errors': 'Password reset link sent to the email address!' })
    return render(request, 'forgot_password.html')


@login_required
def mobile_account(request):
    if request.method == 'POST':
        form = ChangePasswordForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save(commit=True)
    else: form = ChangePasswordForm()
    return render(request, 'mobile/account.html', { 'form': form })

def recover(request, key):
    try:
        ja = Jobseeker_auth.objects.get(auth_code=key)
    except ObjectDoesNotExist:
        raise Http404
    if request.method == 'POST':
        pwd = request.POST.get('password')
        pwd1 = request.POST.get('password1')
        if not pwd or len(pwd) < 8: return render(request, 'recover.html', {'errors': "Password must be at least 8 characters." })
        if not pwd1 or pwd != pwd1: return render(request, 'recover.html', {'errors': "Passwords do not match." })
        ja.verified = True # if they got this email we know their email address is correct
        ja.user.set_password(pwd)
        ja.user.save()
        user = auth.authenticate(username=ja.auth_code)
        auth.login(request, user)
        ja.delete()
        return HttpResponseRedirect('/notice/')
    return render(request, 'recover.html')

def find_results(request):
    search = SearchForm(request.GET)
    if len(request.GET) > 0 and search.is_valid():
        args = []
        kwargs = {}
        text = search.cleaned_data.get('text','')
        if len(text) > 0: args.append(Q(title__contains=text) | Q(body__contains=text) | Q(owner__name__contains=text))
        industries = search.cleaned_data.get('industries')
        if industries: kwargs['industry__in'] = industries
        locations = search.cleaned_data.get('locations')
        if locations: kwargs['location__in'] = locations

        min_years = search.cleaned_data.get('min_years_experience',-1)
        max_years = search.cleaned_data.get('max_years_experience',-1)
        if max_years == -1: max_years = 20
        kwargs['experience__range'] = (min_years, max_years)

        min_degree = search.cleaned_data.get('min_degree',-1)
        max_degree = search.cleaned_data.get('max_degree',-1)
        if max_degree == -1: max_degree = 10
        kwargs['min_degree__range'] = (min_degree, max_degree)

        SavedSearch.objects.create(query=json.dumps({ 'owner': 'jobseeker', 'parameters': {'text': search.cleaned_data.get('text',''), 'other': str(kwargs) }})).save()
        results = JobListing.objects.filter(*args, **kwargs).exclude(end_date__lt=datetime.utcnow().replace(tzinfo=utc))
        for jl in results: 
            jl.views += 1
            jl.save()
        return results
    return []

@login_required
def search(request):
    jobs = list(find_results(request))
    return render(request, 'desktop/notice.html', { 'notices': jobs, 'search_form': SearchForm(request.GET), 'selected': 'search', 'complete': True })

@login_required
def mobile_search(request):
    return render(request, 'mobile/search.html', { 'form': MobileSearchForm() })

@login_required
def mobile_search_results(request):
    return render(request, 'mobile/search_results.html', { 'jobs': find_results(request) })

@login_required
def apply(request, jobId, prefix='desktop'):
    listing = JobListing.objects.get(id=jobId)
    form = ApplyForm(instance=request.user)
    if listing == None:
        return render(request, prefix + '/apply.html', { 'message': 'Invalid Job Listing' })
    items = [process_item_list(request, EducationEventForm, request.user.educationevent_set, 'Education', 'school')]
    vars = { 'listing': listing, 'min_degree': EducationEvent.LEVELS[listing.min_degree][1], 'items':items, 'form': form }
    if request.method == 'POST':
        form = ApplyForm(request.POST, instance=request.user)
        vars['form'] = form
        if form.is_valid():
            form.save(commit=True)
        else:
            return render(request, prefix + '/apply.html', vars)
        try:
            app = Application.objects.create(job=listing,applicant=request.user,cover_letter=request.POST.get('letter'))
            if (listing.number_documents_required > 0 and not request.FILES.get('doc1')) or (listing.number_documents_required > 1 and not request.FILES.get('doc2')):
                return render(request, prefix + '/apply.html', { 'HTTP_REFERER': request.META.get('HTTP_REFERER','/mobile/start/'), 'error': 'Reference documents must be uploaded'})
            try:
                app.save()
            except:
                return render(request, prefix + '/apply.html', { 'HTTP_REFERER': request.META.get('HTTP_REFERER','/mobile/start/'), 'error': 'Reference documents are not of correct file type.'})
            return HttpResponseRedirect('/apply2/' + str(jobId) + '/')
        except:
            return HttpResponseRedirect('/apply2/' + str(jobId) + '/')
    vars['HTTP_REFERER'] = request.META.get('HTTP_REFERER','/mobile/start/')
    vars['NO_MOBILE_REDIRECT'] = True
    return render(request, prefix + '/apply.html', vars)

@login_required
def apply2(request, jobId, prefix='desktop'):
    listing = JobListing.objects.get(id=jobId)
    if listing == None:
        return render(request, prefix + '/apply.html', { 'message': 'Invalid Job Listing' })
    form = SkillForm(instance=request.user)
    items = [ process_item_list(request, CareerEventForm, request.user.careerevent_set, 'Career', 'job') ]
    vars = { 'listing': listing, 'min_degree': EducationEvent.LEVELS[listing.min_degree][1], 'items':items, 'form': form, 'NO_MOBILE_REDIRECT': True }
    if request.method == 'POST':
        form = SkillForm(request.POST, instance=request.user)
        vars['form'] = form
        if form.is_valid():
            form.save(commit=True)
        else:
            return render(request, prefix + '/apply2.html', vars)
        return HttpResponseRedirect('/apply3/' + str(jobId) + '/')
    vars['HTTP_REFERER'] = request.META.get('HTTP_REFERER','/mobile/start/')
    return render(request, prefix + '/apply2.html', vars)

@login_required
def apply3(request, jobId, prefix='desktop'):
    listing = JobListing.objects.get(id=jobId)
    if listing == None:
        return render(request, prefix + '/apply.html', { 'message': 'Invalid Job Listing' })
    items = [process_item_list(request, ReferenceForm, request.user.reference_set, 'Reference', 'reference')]
    form = ApplyFormThree(instance=request.user)
    vars = { 'listing': listing, 'min_degree': EducationEvent.LEVELS[listing.min_degree][1], 'items':items, 'form': form, 'NO_MOBILE_REDIRECT': True }
    if request.method == 'POST':
        form = ApplyFormThree(request.POST, instance=request.user)
        vars['form'] = form
        if form.is_valid():
            form.save(commit=True)
        else:
            return render(request, prefix + '/apply3.html', vars)
        vars['message'] = 'Thank you for applying for this position. You may be contacted by the employer if there is interest in interviewing you. In the meantime, continue to explore other job opportunities you have been matched with from your Notice Board.'
    vars['HTTP_REFERER'] = request.META.get('HTTP_REFERER','/mobile/start/')
    return render(request, prefix + '/apply3.html', vars)

@login_required
def mobile_apply(request, jobId):
    return apply(request, jobId, 'desktop')

def apply_token(request, key, jobId):
    try:
        ja = Jobseeker_auth.objects.get(auth_code=key)
    except ObjectDoesNotExist:
        raise Http404
    ja.user.verified = True
    ja.user.save()
    user = auth.authenticate(username=ja.auth_code)
    auth.login(request, user)
    return HttpResponseRedirect('/apply/' + str(jobId))

def apply_register(request, jobId):
    #if False:
    if request.user.is_authenticated():
        return HttpResponseRedirect('/apply/' + jobId)
    try:
        listing = JobListing.objects.get(id=jobId)
    except ObjectDoesNotExist:
        raise Http404
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid(): # All valiidation rules pass
            form.save(commit=True)
            user = auth.authenticate(username=request.POST['email'], password=request.POST['password'])
            Application.objects.create(applicant=user, job=listing, cover_letter=request.POST.get('cover_letter','')).save()
            auth.login(request, user)
            auth_token = user.generate_token()
            subject = "Welcome to JooMah."
            html_content = ("<div>\r\n"
                            "  <div style='color: #0096C9; background: #FFFFFF; font-size: 3em; padding: 10px;'>J&oacute;&ograve;mah</div>\r\n"
                            "  <div style='background: #0096C9; color: #FFFFFF; font-size: 2em; padding: 10px;'>                                         "
                            "   You successfully applied to your first job on J&oacute;&ograve;mah"
                            "  </div>                               "
                            "</div>\r\n"
                            "<div style='background: #FFFFFF; margin: 30px 60px; padding: 10px;'>\r\n"
                            "  <div style='color: #A5A5A5; font-size: 1.5em;'>To complete your signup, verify your email address by clicking the button below</div>\r\n"
                            "</div>\r\n"
                            "<div style='background: #888888; width: 200px; margin-left: auto; margin-right: auto; color: #FFFFFF; text-align: center; font-size: 3em;          padding: 3px; margin-top: 20px;'>\r\n"
                            "  <a href='http://jobs.joomah.com/verify/"+auth_token.auth_code+"' style='text-decoration: none; color: inherit;'>Verify</a>\r\n"
                            "</div>\r\n")
            html_message = html_head + html_content + html_foot
            from_email, to = 'info@joomah.com', request.POST['email']
            msg = EmailMultiAlternatives(subject, "You successfully applied to your first job on Joomah.  To complete your signup, verify your email address going to http://jobs.joomah.com/verify/"+auth_token.auth_code+" .", from_email, [to])
            msg.attach_alternative(html_message, "text/html")
            msg.send()
            return HttpResponseRedirect('/apply/' + str(jobId) + '/')
    else: form = RegistrationForm()
    return render(request, '/desktop/apply_register.html', { 'job': listing, 'form': form })
    

def mobile_apply_register(request, jobId):
    #if False:
    if request.user.is_authenticated():
        return HttpResponseRedirect('/apply/' + jobId)
    try:
        listing = JobListing.objects.get(id=jobId)
    except ObjectDoesNotExist:
        raise Http404
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid(): # All valiidation rules pass
            form.save(commit=True)
            user = auth.authenticate(username=request.POST['email'], password=request.POST['password'])
            Application.objects.create(applicant=user, job=listing, cover_letter=request.POST.get('cover_letter','')).save()
            auth.login(request, user)
            auth_token = user.generate_token()
            subject = "Welcome to JooMah."
            html_content = ("<div>\r\n"
                            "  <div style='color: #0096C9; background: #FFFFFF; font-size: 3em; padding: 10px;'>J&oacute;&ograve;mah</div>\r\n"
                            "    <div style='background: #0096C9; color: #FFFFFF; font-size: 2em; padding: 10px;'>                                         "
                            "    You successfully applied to your first job on J&oacute;&ograve;mah"
                            "    </div>                               "
                            "</div>\r\n"
                            "<div style='background: #FFFFFF; margin: 30px 60px; padding: 10px;'>\r\n"
                            "  <div style='color: #A5A5A5; font-size: 1.5em;'>To complete your signup, verify your email address by clicking the button below</div>\r\n"
                            "</div>\r\n"
                            "<div style='background: #888888; width: 200px; margin-left: auto; margin-right: auto; color: #FFFFFF; text-align: center; font-size: 3em;          padding: 3px; margin-top: 20px;'>\r\n"
                            "  <a href='http://jobs.joomah.com/verify/"+auth_token.auth_code+"' style='text-decoration: none; color: inherit;'>Verify</a>\r\n"
                            "</div>\r\n")
            html_message = html_head + html_content + html_foot
            text_message = ("You successfully applied to your first job on JooMah,"
                " and your JooMah account is almost ready. "
                "Go to http://jobs.joomah.com/verify/"+auth_token.auth_code+" to verify your account."
                "\n And connect with us on Facebook and Twitter,"
                " and feel free to contact us at info@joomah.com"
                "\nThanks,\nThe JooMah Team.")
            from_email, to = 'info@joomah.com', request.POST['email']
            msg = EmailMultiAlternatives(subject, text_message, from_email, [to])
            msg.attach_alternative(html_message, "text/html")
            msg.send()
            if re.search("mobile",request.META.get('HTTP_REFERER','')):
                return HttpResponseRedirect('/mobile/settings/?initial=True')
            return HttpResponseRedirect('/settings/')
    else: form = RegistrationForm()
    return render(request, '/mobile/apply_register.html', { 'job': listing, 'form': form })

def mobile_apply_token(request, key, jobId):
    try:
        ja = Jobseeker_auth.objects.get(auth_code=key)
    except ObjectDoesNotExist:
        raise Http404
    ja.user.verified = True
    ja.user.save()
    user = auth.authenticate(username=ja.auth_code)
    auth.login(request, user)
    return HttpResponseRedirect('/mobile/apply/' + str(jobId))

@login_required
def schedule(request, job, prefix='desktop'):
    vars = {}
    try:
        listing = JobListing.objects.get(id=job)
        app = Application.objects.get(applicant=request.user,job=listing)
    except:
        return Http404
    if not app.can_interview:
        raise Http404
    if app.interview_time:
        return render(request, prefix + '/schedule.html', {'error': 'Your interview is at ' + str(app.interview_time)})
    cal = json.loads(listing.calendar)
    if request.method == 'POST':
        newCal = []
        for time in cal:
            if time['start'] == request.POST['time']:
                time['title'] = request.user.first_name + ' ' + request.user.last_name
                time['user'] = request.user.id
                app.interview_time = datetime.strptime(request.POST['time'],'%Y-%m-%dT%I:%M:%S.%fZ')
                app.save()
            newCal.append(time)
            listing.calendar = json.dumps(newCal)
            listing.save()
        return HttpResponseRedirect('/')
    times = {}
    for time in cal:
        if not time.get('user'):
            obj = datetime.strptime(time['start'],'%Y-%m-%dT%I:%M:%S.%fZ')
            times[time['start']] = obj.strftime('%d-%m-%Y %I:%M %p')
    return render(request, prefix + '/schedule.html', {'listing': listing, 'times': times})


@login_required
def mobile_schedule(request, job):
    return schedule(request, job, 'mobile')

@login_required
def mobile_start(request):
    incomplete = []
    if request.GET.get('login'):
        if not request.user.skills.exists(): incomplete.append('skills')
        if not request.user.industries.exists(): incomplete.append('industries')
        if not request.user.languages.exists(): incomplete.append('languages')
        if not request.user.location: incomplete.append('location')
        if not request.user.date_of_birth: incomplete.append('date of birth')
        if not request.user.gender: incomplete.append('gender')
        if not request.user.abstract: incomplete.append('description')
    return render(request, 'mobile/start.html', { 'occupation': request.user.occupation(),
        'occupation_history': request.user.careerevent_set.all(),
        'education': request.user.educationevent_set.all(),
        'notices': request.user.notices(),
        'incomplete': incomplete })

def mobile_register(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/mobile/start/')
    return home(request, 'mobile/register.html')

@login_required
def about(request):
    user = request.user
    occupation = user.occupation()
    employer = ''
    if occupation != None:
        employer = occupation.employer_name
        occupation = occupation.job_title
    return render(request, 'desktop/about.html', {
        'occupation': occupation, 'employer': employer,
        'occupation_history': user.careerevent_set.all(),
        'education': user.educationevent_set.all(), 'languages': user.languages.all(),
        'skills': user.skills.all(), 'industries': user.industries.all(),
        'references': user.reference_set.all(),
        'selected': 'about',  })

def process_item_list(request, form, keymanager, title, item_name):
    item_forms = []
    successful = True
    if request and request.method == 'POST':
        number_of_items = int(request.POST.get(item_name + '-number',0) or 0)
        for i in range(0,number_of_items):
            item_form = form(request.POST, prefix=str(i))
            item_forms.append(item_form)
            if not item_form.is_valid(): successful = False
        if successful:
            keymanager.all().delete()
            for iF in item_forms:
                i = iF.save(commit=False)
                i.user = request.user
                i.save()
                keymanager.add(i)
    else:
        i = 0
        for item in keymanager.all():
            item_forms.append(form(prefix=str(i), instance=item))
            i += 1
    
    return {    'forms':        item_forms,
                'sample':       form(),
                'successful':   successful,
                'title':        title,
                'name':         item_name, }

def render_item_list(request, form, keymanager, title, item_name):
    data = process_item_list(request, form, keymanager, title, item_name)
    return render(request, 'item-list.html', {
        'number_of_items': len(data['forms']),
        'item_forms': data['forms'],
        'item_sample': data['sample'],
        'successful': data['successful'],
        'title': data['title'],
        'item_name': data['name'],
        'base': 'mobile/basic.html' })

@login_required
def mobile_education(request):
    return render_item_list(request, EducationEventForm, request.user.educationevent_set, 'Education', 'school')

@login_required
def mobile_career(request):
    return render_item_list(request, CareerEventForm, request.user.careerevent_set, 'Career', 'job')

@login_required
def mobile_references(request):
    return render_item_list(request, ReferenceForm, request.user.reference_set, 'Reference', 'reference')

def home(request, template='desktop/home.html'):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/notice/?first=1')
    if request.method == 'POST': # Form has been submitted
        form = RegistrationForm(request.POST)
        if form.is_valid(): # All valiidation rules pass
            form.save(commit=True)
            user = auth.authenticate(username=request.POST['email'], password=request.POST['password'])
            auth.login(request, user)
            auth_token = user.generate_token()
            subject = "Welcome to JooMah."
            html_content = ("<div>\r\n"
                            "  <div style='color: #0096C9; background: #FFFFFF; font-size: 3em; padding: 10px;'>J&oacute;&ograve;mah</div>\r\n"
                            "    <div style='background: #0096C9; color: #FFFFFF; font-size: 2em; padding: 10px;'>                                         "
                            "    Your J&oacute;&ograve;mah account is almost ready"
                            "    </div>                               "
                            "</div>\r\n"
                            "<div style='background: #FFFFFF; margin: 30px 60px; padding: 10px;'>\r\n"
                            "  <div style='color: #A5A5A5; font-size: 1.5em;'>To complete your signup, verify your email address by clicking the button below</div>\r\n"
                            "</div>\r\n"
                            "<div style='background: #888888; width: 200px; margin-left: auto; margin-right: auto; color: #FFFFFF; text-align: center; font-size: 3em;          padding: 3px; margin-top: 20px;'>\r\n"
                            "  <a href='http://jobs.joomah.com/verify/"+auth_token.auth_code+"' style='text-decoration: none; color: inherit;'>Verify</a>\r\n"
                            "</div>\r\n")
            html_message = html_head + html_content + html_foot
            text_message = ("Your JooMah account is almost ready.  Go to http://jobs.joomah.com/verify/"+auth_token.auth_code+" to verify your account."
                "\n And connect with us on Facebook and Twitter, and feel free to contact us at info@joomah.com"
                "\nThanks,\nThe JooMah Team.")
            from_email, to = 'info@joomah.com', request.POST['email']
            msg = EmailMultiAlternatives(subject, text_message, from_email, [to])
            msg.attach_alternative(html_message, "text/html")
            msg.send()
            if re.search("mobile",request.META.get('HTTP_REFERER','')):
                return HttpResponseRedirect('/mobile/settings/?initial=True')
            return HttpResponseRedirect('/settings/')
    else:
        form = RegistrationForm()
    jobs = JobListing.objects.order_by('-date_created')[:5]
    return render(request, template, { 'form': form, 'retries': request.GET.get('retries', 0), 'jobs': jobs, 'next': request.GET.get('next','') })

@login_required
def resume(request):
    return render_to_pdf('desktop/resume.html', {
        'user': request.user,
        'occupation': request.user.occupation(),
        'selected': 'about'  })

def render_to_pdf(template_src, context_dict):
    template = get_template(template_src)
    context = Context(context_dict)
    html  = template.render(context)
    result = BytesIO()
    htm = html.encode("ISO-8859-1","xmlcharrefreplace")
    string = BytesIO(htm)
    pdf = pisa.pisaDocument(string, result)
    if not pdf.err:
        return HttpResponse(result.getvalue(), content_type='application/pdf')
    return HttpResponse('We had some errors<pre>%s</pre>' % escape(html))

@login_required
def settings(request):
    user_country = request.user.location or None
    if user_country:
        user_country = Location.objects.get(city__isnull=True, country=user_country.country)
    successful = request.method == 'POST'
    name = request.POST.get('name')
    order = ['main','school','job','personality']
    form = EditJobseekerForm(instance=request.user)
    personality_form = PersonalityQuizForm()
    password_form = ChangePasswordForm()
    items = [   process_item_list(( request if name == 'school' else None), EducationEventForm, request.user.educationevent_set, 'Education', 'school'),
                process_item_list(( request if name == 'job' else None), CareerEventForm, request.user.careerevent_set, 'Career', 'job'),
                process_item_list(( request if name == 'reference' else None), ReferenceForm, request.user.reference_set, 'Reference', 'reference')]
    if name == 'main':
        form = EditJobseekerForm(request.POST, request.FILES, instance=request.user)
        successful = False
        if form.is_valid(): 
            successful = True
            form.save(commit=True)
            form = EditJobseekerForm(instance=request.user)
    elif name == 'password':
        password_form = ChangePasswordForm(request.POST, instance=request.user)
        successful = False
        if password_form.is_valid():
            successful = True
            password_form.save(commit=True)
            password_form = ChangePasswordForm()
    elif name == 'personality':
        personality_form = PersonalityQuizForm(request.POST)
        successful = False
        if personality_form.is_valid():
            successful = True
            request.user.enneagram_results = personality_form.score()
            request.user.save()
    initial = request.POST.get('initial') or (True if re.search("login",request.META.get('HTTP_REFERER','')) else False)
    initial = initial or re.search("register",request.META.get('HTTP_REFERER',''))
    successful = successful and all([x['successful'] for x in items])
    if initial and successful:
        if name == 'main':
            name = 'school'
        elif name == 'school':
            name = 'job'
        elif name == 'job':
            name = 'personality'
        elif name == 'personality':
            return HttpResponseRedirect('/')
    elif successful:
        return HttpResponseRedirect('/about/')
    if user_country:
        form.fields['location'].queryset = Location.objects.filter(country=user_country.country, city__isnull=False)
        form.fields['languages'].queryset = Language.objects.filter(Q(country=user_country) | Q(country__isnull=True))
    return render(request, 'desktop/settings.html', {
        'main_form': form,
        'personality_form': personality_form,
        'password_form': password_form,
        'default_page': name,
        'items': items,
        'successful': successful, 
        'selected': 'settings',
        'initial': initial
    })

@login_required
def mobile_quiz(request):
    if request.method == 'POST':
        form = PersonalityQuizForm(request.POST)
        if form.is_valid():
            request.user.enneagram_results = form.score()
            request.user.save()
    else:
        form = PersonalityQuizForm()
    return render(request, 'mobile/quiz.html', { 'form': form })

@login_required
def mobile_settings(request):
    successful = False
    if request.method == 'POST':
        form = MobileEditJobseekerForm(request.POST.copy(), request.FILES, instance=request.user)
        if form.is_valid():
            successful = True
            form.save(commit=True)
            form = MobileEditJobseekerForm(instance=request.user)
        if request.POST.get('initial'):
            return HttpResponseRedirect('/mobile/start/')
    else:
        form = MobileEditJobseekerForm(instance=request.user)
    if request.GET.get('initial',False):
        return render(request, 'mobile/settings.html', { 'form': form, 'successful': successful, 'initial': True })
    if successful:
        return HttpResponseRedirect('/mobile/start/')
    return render(request, 'mobile/settings.html', { 'form': form, 'successful': successful })

def blog(request, prefix='desktop'):
    return render(request, 'blog.html', {'base': prefix + '/basic.html',
        'entries': BlogEntry.objects.all(), 'selected': 'blog'})
def blog_entry(request, key, prefix='desktop'):
    try:
        entry = BlogEntry.objects.get(id=key)
    except ObjectDoesNotExist:
        raise Http404
    return render(request, 'blog-entry.html', { 'base': prefix + '/basic.html',
        'entry': entry, 'selected': 'blog' })

def mobile_blog(request): return blog(request, prefix='mobile')
def mobile_blog_entry(request, key): return blog_entry(request, key, prefix='mobile')

def blog_post(request):
    if request.method == 'POST':
        form = BlogEntryForm(request.POST)
        if form.is_valid():
            form.save(commit=True)
            form = BlogEntryForm()
    else:
        form = BlogEntryForm()
    return render(request, 'desktop/blog-post.html', { 'form': form })
