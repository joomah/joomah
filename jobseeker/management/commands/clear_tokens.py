from django.core.management.base import BaseCommand
from django.utils.timezone import utc
from common.models import Jobseeker_auth
from datetime import datetime, timedelta

class Command(BaseCommand):
    def handle(self, *args, **options):
        Jobseeker_auth.objects.filter(creation_date__lte=datetime.utcnow().replace(tzinfo=utc) - timedelta(days=7)).delete()
        self.stdout.write("Old tokens cleared!")