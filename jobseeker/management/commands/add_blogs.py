from django.core.management.base import BaseCommand
from django.db.models import Max
from common.models import BlogEntry
import MySQLdb

class Command(BaseCommand):
    def handle(self, *args, **options):
        blogs = BlogEntry.objects.all()
        if len(blogs) > 0: max_id = blogs.aggregate(Max('key_in_wp'))['key_in_wp__max']
        else: max_id = -1
        db = MySQLdb.connect(host="localhost", # your host, usually localhost
                     user="blog", # your username
                      passwd="blogisablog123123", # your password
                      db="joomah_blog") # name of the data base
        cur = db.cursor()
        cur.execute("SELECT ID,post_title,post_content FROM wp_posts WHERE post_type='post' AND post_status='publish' AND ID > " + str(max_id))
        i = 0
        for row in cur.fetchall():
            i += 1
            BlogEntry.objects.create(key_in_wp=row[0],title=unicode(row[1],'latin1').encode('utf8'),content=unicode(row[2],'latin1').encode('utf8')).save()
        self.stdout.write("Successfully added " + str(i) + " blog posts from WordPress.")
