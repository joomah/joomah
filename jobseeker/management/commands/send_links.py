from django.core.management.base import BaseCommand
from common.models import Jobseeker, ExtensionChange, JobListing
from django.db.models import Q
from django.core.files import File
import subprocess, os, urllib
import re
from os.path import basename
from django.core.mail import send_mail, EmailMultiAlternatives

html_head = ("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>\r\n"
             "<html xmlns='http://www.w3.org/1999/xhtml'>\r\n"
             "<head>\r\n"
             "<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />\r\n"
             "<meta name='description' content=\"J&oacute;&ograve;Mah, the place to find jobs near you. Join Top Jobs in Ghana - Ghana's Leading Job Site.\">\r\n"
             "<title>J&oacute;&ograve;Mah: Applicant Screening Made Easy</title>\r\n"
             "<link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>\r\n"
             "</head>\r\n"
             "<body style='background: #ECF0F1; font-size: 12px;'>\r\n"
             "<div style='width: 60%; min-width: 400px; max-width: 800px; margin-left: auto; margin-right: auto; color: gray; margin-top: 40px;'>\r\n")
html_foot = ("  <div style='color: gray;width: 200px; min-width: 600px; margin-left: auto; margin-right: auto; margin-top: 100px; text-align: center;'>\r\n"
             "    <div>\r\n"
             "      <span style='text-decoration: none; color: inherit; margin: 10px;'>Tell you friends and colleagues about us on "
             "      <a href='https://www.facebook.com/pages/J%C3%B3%C3%B2Mah/827539130595240' style='text-decoration: none; color: #2FA8D1;'>Facebook</a> or "
             "      <a href='https://twitter.com/JoomahAfrica' style='text-decoration: none; color: #2FA8D1;'>Twitter</a></span>\r\n"
             "    </div>\r\n"
             "    <div>\r\n"
             "      <a href='http://jobs.joomah.com/terms' style='text-decoration: none; color: #2FA8D1; margin: 10px;'>Privacy Policy</a>\r\n"
             "      <a href='mailto:info@joomah.com' style='text-decoration: none; color: #2FA8D1; margin: 10px'>Contact us</a>\r\n"
             "    </div>\r\n"
             "  </div>\r\n"
             "</div>\r\n"
             "</body>\r\n"
             "</html>\r\n")

class Command(BaseCommand):
    def handle(self, *args, **options):
        mailfile = open('/home/vmail/Maildir', 'r')
        read_data = mailfile.read()
        mailfile.close()

        open('/home/vmail/Maildir', 'w').close()

        lines = read_data.split('\n')
        tos = filter(lambda x: x.startswith('To:'), lines)
        froms = filter(lambda x: x.startswith('From:'), lines)

        groups = zip(tos, froms)

        for (to,fro) in groups:
            try:
                to_addr = re.search(r'[\w\.~]+@[\w\.-]+', to).group(0) 
                from_addr = re.search(r'[\w\.~]+@[\w\.-]+', fro).group(0)
                job_id = re.search(r'(\d+)', to_addr).groups(0)[0]
                job = JobListing.objects.get(id=job_id)
                subject = "Complete your Application!"
                html_content = ("<div>\r\n"
                                "  <div style='color: #0096C9; background: #FFFFFF; font-size: 3em; padding: 10px;'>J&oacute;&ograve;mah</div>\r\n"
                                "    <div style='background: #0096C9; color: #FFFFFF; font-size: 2em; padding: 10px;'>                                         "
                                "    Complete your job application for " + job.title + ""
                                "    </div>                               "
                                "</div>\r\n"
                                "<div style='background: #FFFFFF; margin: 30px 60px; padding: 10px;'>\r\n"
                                "  <div style='color: #A5A5A5; font-size: 1.5em;'>You have started applying for " + job.title + ". To complete the registration process, click the 'Apply' button below.</div>\r\n"
                                "</div>\r\n"
                                "<div style='background: #888888; width: 200px; margin-left: auto; margin-right: auto; color: #FFFFFF; text-align: center; font-size: 3em;          padding: 3px; margin-top: 20px;'>\r\n"
                                "  <a href='http://jobs.joomah.com/apply/register/"+job_id+"/' style='text-decoration: none; color: inherit;'>Apply</a>\r\n"
                                "</div>\r\n")
                html_message = html_head + html_content + html_foot
                text_message = ("Your application is almost ready.  Go to http://jobs.joomah.com/apply/register/"+ job_id + '/' +" to verify your account."
                    "\n And connect with us on Facebook and Twitter, and feel free to contact us at info@joomah.com"
                    "\nThanks,\nThe JooMah Team.")
                from_email, to = 'info@joomah.com', from_addr
                msg = EmailMultiAlternatives(subject, text_message, from_email, [to])
                msg.attach_alternative(html_message, "text/html")
                msg.send()
            except:
                pass

