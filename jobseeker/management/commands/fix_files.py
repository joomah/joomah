from django.core.management.base import BaseCommand
from common.models import Jobseeker, ExtensionChange
from django.db.models import Q
from django.core.files import File
import subprocess, os, urllib
from os.path import basename

class Command(BaseCommand):
    def handle(self, *args, **options):
        to_fix = ExtensionChange.objects.all()
        i = 0
        for e in to_fix:
            j = e.jobseeker
            filename = urllib.unquote(j.resume.url)
            baseName = os.path.splitext(basename(filename))[0]
            fullPath = '/srv/test.joomah.com/jobseeker' + filename
            name, ext = os.path.splitext(fullPath)
            subprocess.call(["/usr/bin/unoconv", "-f", "pdf", fullPath])
            pdfFile = open(name + '.pdf') 
            j.resume.save('CV.pdf', File(pdfFile))
            os.remove(fullPath)
            os.remove(name + '.pdf')
            pdfFile.close()
            j.save()
            i += 1
        ExtensionChange.objects.all().delete()
        self.stdout.write(str(i) + " bad files fixed.")
