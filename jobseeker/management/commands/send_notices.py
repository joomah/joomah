from django.core.management.base import BaseCommand
from django.core.mail import send_mail, EmailMultiAlternatives
from common.models import Jobseeker
import re

html_head = ("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>\r\n"
             "<html xmlns='http://www.w3.org/1999/xhtml'>\r\n"
             "<head>\r\n"
             "<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />\r\n"
             "<title>J&oacute;&ograve;Mah: Applicant Screening Made Easy</title>\r\n"
             "<link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>\r\n"
             "</head>\r\n"
             "<body style='background: #ECF0F1; font-size: 12px;'>\r\n"
             "<div style='width: 60%; min-width: 400px; max-width: 800px; margin-left: auto; margin-right: auto; color: gray; margin-top: 40px;'>\r\n")
html_foot = ("  <div style='color: gray;width: 200px; min-width: 600px; margin-left: auto; margin-right: auto; margin-top: 100px; text-align: center;'>\r\n"
             "    <div>\r\n"
             "      <span style='text-decoration: none; color: inherit; margin: 10px;'><a href='https://jobs.joomah.com/unsubscribe/'>Unsubscribe</a></span>"
             "    </div>\r\n"
             "    <div>\r\n"
             "      <span style='text-decoration: none; color: inherit; margin: 10px;'>Tell you friends and colleagues about us on "
             "      <a href='https://www.facebook.com/pages/J%C3%B3%C3%B2Mah/827539130595240' style='text-decoration: none; color: #2FA8D1;'>Facebook</a> or "
             "      <a href='https://twitter.com/JoomahAfrica' style='text-decoration: none; color: #2FA8D1;'>Twitter</a></span>\r\n"
             "    </div>\r\n"
             "    <div>\r\n"
             "      <a href='http://jobs.joomah.com/terms' style='text-decoration: none; color: #2FA8D1; margin: 10px;'>Privacy Policy</a>\r\n"
             "      <a href='mailto:info@joomah.com' style='text-decoration: none; color: #2FA8D1; margin: 10px'>Contact us</a>\r\n"
             "    </div>\r\n"
             "  </div>\r\n"
             "</div>\r\n"
             "</body>\r\n"
             "</html>\r\n")

class Command(BaseCommand):
    def handle(self, *args, **options):
        jobseekers = Jobseeker.objects.all()
        for jobseeker in jobseekers:
            text_message = (jobseeker.first_name + ", here's what's going on at jobs.joomah.com\r\n\r\n")
            html_content = ("<div>\r\n"
                            "  <div style='color: #0096C9; background: #FFFFFF; font-size: 3em; padding: 10px;'>J&oacute;&ograve;mah </div>\r\n"
                            "  <div style='background: #0096C9; color: #FFFFFF; font-size: 2em; padding: 10px;'>\r\n"
                            "    <span>" + jobseeker.first_name + "</span>, Weekly Joomah Summary"
                            "  </div>\r\n"
                            "</div>\r\n")
            notices = jobseeker.notices()
            if len(notices) == 0:
                continue
            new_token = jobseeker.generate_token()
            text_message += ("See who wants to hire you:\r\n")
            for job in notices[:10]:
                html_content += ("<div style='background: #FFFFFF; margin: 20px 50px; padding: 20px;'><a href='https://jobs.joomah.com/apply/token/" + str(new_token.auth_code) + "/" + str(job.id) + "/'>\r\n"
                                 "<div style='color: #0096C9; font-size: 1.5em;'>" + job.owner.name + " is hiring a " + job.title + "</div>\r\n")
                if job.end_date: html_content += "<div>Deadline: " + job.end_date.strftime('%d-%m-%Y') + "</div>\r\n"
                new_token = jobseeker.generate_token()
                text_message += ("\t" + job.owner.name + " is hiring a " + job.title + ". Apply at https://jobs.joomah.com/apply/token/" + str(new_token.auth_code) + "/" + str(job.id) + "/\r\n")
                html_content += "<div><a href='https://jobs.joomah.com/apply/token/" + str(new_token.auth_code) + "/" + str(job.id) + "/' style='text-decoration: none; color: #2FA8D1;'>Apply</a></div></a></div>\r\n"

            text_message += ("\r\nThanks,\r\n\tThe Joomah Team")
            html_content += ("</div>\r\n"
                             "<div style='background: #0096C9; width: 200px; margin-left: auto; margin-right: auto; color: #FFFFFF; text-align: center; font-size: 3em; padding: 3px; margin-top: 20px;'>\r\n"
                             "<a href='https://jobs.joomah.com' style='text-decoration: none; color: inherit;'>See More</a>\r\n"
                             "</div>\r\n")
            html_message = html_head + html_content + html_foot
            msg = EmailMultiAlternatives("JooMah Job Notices", text_message, 'info@joomah.com', [jobseeker.email])
            msg.attach_alternative(html_message, "text/html")
            msg.send()
        self.stdout.write("Successfully added notices to message queue.")
