from django.conf.urls import patterns, include, url
from django.views.generic import RedirectView
import common.urls
import twilio_views.urls
import metrics.urls
import jobseeker.settings
from jobseeker import views

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^favicon\.ico$', RedirectView.as_view(url='/static/img/favicon.ico', permanent=True)),
    url(r'^unsubscribe/$', 'jobseeker.views.unsubscribe'),
    url(r'^$', 'jobseeker.views.home'),
    url(r'^login/$', 'jobseeker.views.home'),
    url(r'^settings/$', 'jobseeker.views.settings'),
    url(r'^about/$', 'jobseeker.views.about'),
    url(r'^search/$', 'jobseeker.views.search'),
    url(r'^notice/$', 'jobseeker.views.notice'),
    url(r'^job-history/$', 'jobseeker.views.job_history'),
    url(r'^apply/([0-9]+)/$', 'jobseeker.views.apply'),
    url(r'^apply2/([0-9]+)/$', 'jobseeker.views.apply2'),
    url(r'^apply3/([0-9]+)/$', 'jobseeker.views.apply3'),
    url(r'^apply/token/(.+)/([0-9]+)/$', 'jobseeker.views.apply_token'),
    url(r'^apply/register/([0-9]+)/$', 'jobseeker.views.apply_register'),
    url(r'^schedule/([0-9]+)/$', 'jobseeker.views.schedule'),
    url(r'^verify/$', 'jobseeker.views.send_verification'),
    url(r'^verify/(.+)/$', 'jobseeker.views.verify'),
    url(r'^recover/(.+)/$', 'jobseeker.views.recover'),
    url(r'^recover/$', 'jobseeker.views.forgot_password'),
    url(r'^terms/$', 'jobseeker.views.terms'),
    url(r'^resume/$', 'jobseeker.views.resume'),
    #url(r'^blog/$','jobseeker.views.blog'),
    #url(r'^blog/([0-9]+)$','jobseeker.views.blog_entry'),
    #url(r'^blog/post/$', 'jobseeker.views.blog_post'),
    url(r'^about-us/$', 'jobseeker.views.about_joomah'),
    url(r'^mobile/$', 'jobseeker.views.mobile_landing'),
    url(r'^mobile/signin/$', 'jobseeker.views.mobile_signin'),
    url(r'^mobile/home/$', 'jobseeker.views.mobile_home'),
    url(r'^mobile/start/$', 'jobseeker.views.mobile_start'),
    url(r'^mobile/register/$', 'jobseeker.views.mobile_register'),
    url(r'^mobile/notice/$', 'jobseeker.views.mobile_notice'),
    url(r'^mobile/job-history/$', 'jobseeker.views.mobile_job_history'),
    url(r'^mobile/login/$', 'jobseeker.views.mobile_signin'),
    url(r'^mobile/education/$', 'jobseeker.views.mobile_education'),
    url(r'^mobile/references/$', 'jobseeker.views.mobile_references'),
    url(r'^mobile/settings/$', 'jobseeker.views.mobile_settings'),
    url(r'^mobile/account/$', 'jobseeker.views.mobile_account'),
    url(r'^mobile/career/$', 'jobseeker.views.mobile_career'),
    url(r'^mobile/quiz/$', 'jobseeker.views.mobile_quiz'),
    url(r'^mobile/search/$', 'jobseeker.views.mobile_search'),
    url(r'^mobile/search/results/$', 'jobseeker.views.mobile_search_results'),
    url(r'^mobile/apply/([0-9]+)/$', 'jobseeker.views.mobile_apply'),
    url(r'^mobile/apply/token/(.+)/([0-9]+)/$', 'jobseeker.views.mobile_apply_token'),
    url(r'^mobile/apply/register/([0-9]+)/$', 'jobseeker.views.mobile_apply_register'),
    url(r'^mobile/schedule/([0-9]+)/$', 'jobseeker.views.mobile_schedule'),
    url(r'^mobile/terms/$', 'jobseeker.views.mobile_terms'),
    #url(r'^mobile/blog/$','jobseeker.views.mobile_blog'),
    #url(r'^mobile/blog/([0-9]+)$','jobseeker.views.mobile_blog_entry'),
)

urlpatterns += common.urls.urlpatterns
urlpatterns += twilio_views.urls.urlpatterns
urlpatterns += metrics.urls.urlpatterns

if not jobseeker.settings.DEBUG:
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns
    #urlpatterns += patterns('',
                        #(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
                        #)
    
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': jobseeker.settings.MEDIA_ROOT}))

else:
    pass
