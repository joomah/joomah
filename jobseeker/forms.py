# -*- coding: utf-8 -*-
from django import forms
from django.forms import ModelForm, ValidationError, CheckboxSelectMultiple
from django.forms.widgets import Select, TextInput, RadioSelect, SelectMultiple
from django.forms.extras.widgets import SelectDateWidget
from django.contrib.auth.forms import UserCreationForm
from common.models import JobListing, Jobseeker, Industry, CareerEvent, EducationEvent, Location, BlogEntry, Reference
from common.widgets import SelectMonthYearWidget
from PIL import Image
from datetime import datetime
import os

class BlogEntryForm(ModelForm):
    class Meta:
        model = BlogEntry
        exclude = ('date_created',)

class RegistrationForm(ModelForm): 
    password1 = forms.CharField(label='Verify Password', max_length=30,widget=forms.PasswordInput(attrs={'placeholder': 'Verify Password'}))
    class Meta: 
        model = Jobseeker
        fields = ('first_name','last_name','email','password',)
        widgets = { 'password': forms.PasswordInput(attrs={'placeholder': 'Password'}), }
    def __init__(self, *args, **kwargs):
        super(RegistrationForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].widget.attrs = { 'placeholder': 'First Name' }
        self.fields['last_name'].widget.attrs = { 'placeholder': 'Last Name' }
        self.fields['email'].widget.attrs = { 'placeholder': 'Email Address'}
    def clean(self):
        cleaned_data = super(RegistrationForm, self).clean()
        password = cleaned_data.get('password')
        password1 = cleaned_data.get('password1')
        if password and password1:
            if password != password1:
                raise ValidationError("Passwords do not match.")
            if len(password) < 8:
                raise ValidationError("Password must be at least 8 characters.")
        else:
            raise ValidationError("Please enter a password.")
        return cleaned_data

    def save(self, commit=True):
        user = super(RegistrationForm, self).save(commit=False) 
        user.set_password(self.cleaned_data['password']) 
        user.is_active = True
        if commit: user.save(initial_pwd=True) 
        return user 

class ApplyForm(ModelForm):
    class Meta:
        model = Jobseeker
        fields = ('location','languages',)
        widgets = {
                'languages': CheckboxSelectMultiple()
                }
    def __init__(self, *args, **kwargs):
        super(ApplyForm, self).__init__(*args, **kwargs)
        self.fields['languages'].help_text = ''

    def save(self, commit=False):
        jobseeker = super(ApplyForm, self).save(commit=False)
        if commit:
            jobseeker.save()
            self.save_m2m()
        return jobseeker

class SkillForm(ModelForm):
    class Meta:
        model = Jobseeker
        fields = ('skills',)
        widgets = {
                'skills': CheckboxSelectMultiple()
                }
    def __init__(self, *args, **kwargs):
        super(SkillForm, self).__init__(*args, **kwargs)
        self.fields['skills'].help_text = ''

    def save(self, commit=False):
        jobseeker = super(SkillForm, self).save(commit=False)
        if commit:
            jobseeker.save()
            self.save_m2m()
        return jobseeker

class ApplyFormThree(ModelForm):
    country_code = forms.ChoiceField(widget=Select(),choices=[('+233','Ghana'),('+234','Nigeria')])
    class Meta:
        model = Jobseeker
        fields = ('abstract','phone_number',)

    def __init__(self, *args, **kwargs):
        super(ApplyFormThree, self).__init__(*args, **kwargs)
        if self.data.get('phone_number'):
            self.data['phone_number'] = self.data.get('country_code','+233') + self.data.get('phone_number','')
        elif self.initial.get('phone_number'):
            self.initial['country_code'] = str(self.initial.get('phone_number',''))[:-9]
            self.initial['phone_number'] = '0' + str(self.initial.get('phone_number',''))[-9:]
        self.fields['abstract'].label = 'Describe Yourself'

    def clean(self): 
        cleaned_data = super(ApplyFormThree, self).clean()
        if self.errors:
            self.data['country_code'] = self.data.get('phone_number','')[:4]
            self.data['phone_number'] = self.data.get('phone_number','')[4:]
        return cleaned_data

class EditJobseekerForm(ModelForm):
    country_code = forms.ChoiceField(widget=Select(),choices=[('+233','Ghana'),('+234','Nigeria')])
    class Meta:
        model = Jobseeker
        exclude = ('last_login','verified','connections','password','enneagram_results',)
        widgets = {
            'date_of_birth': SelectDateWidget(years=[x for x in range(datetime.now().year - 15, datetime.now().year - 80,-1)]),
            'industries': CheckboxSelectMultiple(),
            'languages': CheckboxSelectMultiple(),
            'skills': CheckboxSelectMultiple(),
            'abstract': forms.Textarea(attrs={'placeholder': 'Describe yourself . . .'}),
            }
    def __init__(self, *args, **kwargs):
        super(EditJobseekerForm, self).__init__(*args, **kwargs)
        if self.data.get('phone_number'):
            self.data['phone_number'] = self.data.get('country_code','+233') + self.data.get('phone_number','')
        elif self.initial.get('phone_number'):
            self.initial['country_code'] = str(self.initial.get('phone_number',''))[:-9]
            self.initial['phone_number'] = '0' + str(self.initial.get('phone_number',''))[-9:]
        self.fields['industries'].help_text = ''
        self.fields['languages'].help_text = ''
        self.fields['skills'].help_text = ''
        self.fields['abstract'].label = 'Describe Yourself'

    def clean_profile_photo(self):
        image = self.cleaned_data.get('profile_photo', None)
        if not image: return None
        try:
            size = 200, 200
            image.open('r+')
            im = Image.open(image,"r")
            im.thumbnail(size, Image.ANTIALIAS)
            im.save(image)
        except (OSError,IOError): pass
        except: pass
        return image
    def clean(self): 
        cleaned_data = super(EditJobseekerForm, self).clean()
        if self.errors:
            self.data['country_code'] = self.data.get('phone_number','')[:4]
            self.data['phone_number'] = self.data.get('phone_number','')[4:]
        return cleaned_data
           
    def save(self, commit=False):
        jobseeker = super(EditJobseekerForm, self).save(commit=False)
        if commit:
            jobseeker.save()
            self.save_m2m()
        return jobseeker

class MobileEditJobseekerForm(EditJobseekerForm):
    class Meta:
        model = Jobseeker
        exclude = ('last_login','verified','connections','password','enneagram_results',)
        widgets = { 'date_of_birth': SelectDateWidget(years=[x for x in range(datetime.now().year-16,datetime.now().year - 100,-1)]), }

class ChangePasswordForm(forms.Form):
    password = forms.CharField(widget=forms.PasswordInput, label="Change Password")
    password1 = forms.CharField(widget=forms.PasswordInput, label="Verify Password")
    def __init__(self, *args, **kwargs):
        self.instance = kwargs.pop('instance',None)
        super(ChangePasswordForm, self).__init__(*args, **kwargs)
    def clean_password1(self):
        password = self.cleaned_data.get('password','')
        password1 = self.cleaned_data.get('password1','')
        if password != password1:
            raise ValidationError("Passwords do not match.")
        if len(password) < 8:
            raise ValidationError("Password must be at least 8 characters.")
        return password1
    def save(self, commit=True):
        self.instance.set_password(self.cleaned_data['password'])
        if commit: self.instance.save()


class JobListingForm(ModelForm):
    class Meta:
        model = JobListing
        exclude = ('owner',)

class EducationEventForm(ModelForm):
    class Meta:
        model = EducationEvent
        fields = ('start_date','end_date','institution_name','level','qualification','location','activities','honors',)
        widgets = {
            'start_date': Select(choices=[(x,x) for x in range(datetime.now().year,datetime.now().year - 80,-1)],attrs={'class': 'start_date',}),
            'end_date': Select(choices=[('','Present')]+[(x,x) for x in range(datetime.now().year,datetime.now().year - 80,-1)],attrs={'class': 'end_date',}),
        }
    def __init__(self, *args, **kwargs):
        super(EducationEventForm, self).__init__(*args, **kwargs)
        self.fields['institution_name'].widget.attrs = { 'placeholder': 'Institution Name' }
        self.fields['qualification'].widget.attrs = { 'placeholder': 'Major or Specialization'}
        self.fields['location'].widget.attrs = { 'placeholder': 'Location' }
        self.fields['activities'].widget.attrs = { 'placeholder': 'Activities' }
        self.fields['honors'].widget.attrs = { 'placeholder': 'Honors' }
    def clean_end_date(self):
        start_date = self.cleaned_data.get('start_date')
        end_date = self.cleaned_data.get('end_date',None)
        if end_date and start_date > end_date:
            raise ValidationError("Graduation year must be after start year.")
        return end_date

class ReferenceForm(ModelForm):
    class Meta:
        model = Reference
        fields = ['name','address','phone_number','email']

    def __init__(self, *args, **kwargs):
        super(ReferenceForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs = { 'placeholder': 'Name' }
        self.fields['address'].widget.attrs = { 'placeholder': 'Address' }
        self.fields['phone_number'].widget.attrs = { 'placeholder': 'Phone Number' }
        self.fields['email'].widget.attrs = { 'placeholder': 'Email' }

class CareerEventForm(ModelForm):
    class Meta:
        model = CareerEvent
        fields = ('start_date','end_date','employer_name','industry','job_title','job_description',)
        widgets = {
            'start_date': SelectMonthYearWidget(years=[x for x in range(datetime.now().year,datetime.now().year - 80,-1)]),
            'end_date': SelectMonthYearWidget(years=[x for x in range(datetime.now().year,datetime.now().year - 80,-1)])
        }
    def __init__(self, *args, **kwargs):
        super(CareerEventForm, self).__init__(*args, **kwargs)
        self.fields['employer_name'].widget.attrs = { 'placeholder': 'Employer Name' }
        self.fields['job_title'].widget.attrs = { 'placeholder': 'Job Title' }
        self.fields['job_description'].widget.attrs = { 'placeholder': 'Description' }

class SearchForm(forms.Form):
    text = forms.CharField(required=False, label="")
    min_years_experience = forms.IntegerField(widget=Select(choices=[(-1,'----')] + [(x,x) for x in range(0,10)]), required=False)
    max_years_experience = forms.IntegerField(widget=Select(choices=[(-1,'----')] + [(x,x) for x in range(0,10)]), required=False)
    min_degree = forms.IntegerField(widget=Select(choices=((-1,'----'),) + EducationEvent.LEVELS), required=False)
    max_degree = forms.IntegerField(widget=Select(choices=((-1,'----'),) + EducationEvent.LEVELS), required=False)
    industries = forms.ModelMultipleChoiceField(Industry.objects, widget=CheckboxSelectMultiple(), required=False)
    locations = forms.ModelMultipleChoiceField(Location.objects, widget=CheckboxSelectMultiple(), required=False)
    def __init__(self, *args, **kwargs):
        super(SearchForm, self).__init__(*args, **kwargs)
        self.fields['text'].widget.attrs = { 'placeholder': 'Job Title or Employer Name'}
        self.fields['industries'].help_text = ''
        self.fields['locations'].help_text = ''

    def clean_max_years_experience(self):
        min_years = self.cleaned_data.get('min_years_experience')
        max_years = self.cleaned_data.get('max_years_experience')
        if min_years and max_years and max_years > -1 and min_years > max_years:
            raise ValidationError("Maximum years experience must be greater than minimum")
        return max_years
    def clean_max_degree(self):
        min_degree = self.cleaned_data.get('min_degree')
        max_degree = self.cleaned_data.get('max_degree')
        if min_degree and max_degree and max_degree > -1 and min_degree > max_degree:
            raise ValidationError("Maximum degree must be greater than minimum")
        return max_degree

class MobileSearchForm(SearchForm):
    industries = forms.ModelMultipleChoiceField(Industry.objects, required=False)
    locations = forms.ModelMultipleChoiceField(Location.objects, required=False)

class PersonalityQuizForm(forms.Form):
    QUESTIONS = [ ("He is too important for tolerating any delay.", [(0,"to tolerate"),(1,"to tolerating"),(2,"at tolerating"),(3,"No correction required")]),
                  ("If the room had been brighter, I would have been able to read for a while before bedtime.", [(0,"If the room was brighter"),(1,"If the room are brighter"),(2,"Had the room been brighter"),(3,"No correction required")]),
                  ("What is the missing number? 83 - 17 = 56 + ?", [(0,"6"), (1,"7"), (2,"10"), (3,"27")]),
                  ("If Kofi is older than Ama and Edem is older than Kofi, can Ama be older than Edem?", [(0,"Yes"), (1,"No"), (2,"Uncertain")]),
                  ("Which one of the following is spelled properly?", [(0,"Receive"),(1,"Recieve"),(2,"Decieve"),(3,"Deiceve")]),
                  ("A fruit seller had some apples. He sells 40% and still has 420 apples left.  Orginally, he had:", [(0,"588"),(1,"600"),(2,"672"),(3,"700")]),
                  ("Look at this series: 3,6,9,12,15,... What number should come next?", [(0,"16"),(1,"18"),(2,"21"),(3,"24")]),
                  ("Pen is to poet as needle is to...", [(0,"Writer"),(1,"Thread"),(2,"Tailor"),(3,"Sewing")]),
                  ("Statement 1 is 'The farmers have decided against selling their cash crops to the government'. Statement 2 is 'The government reduced the price of cash crops for the next six months.'", [(0,"Statement 1 is the cause and Statement 2 is the effect."), (1,"Statement 2 is the cause and Statement 1 is the effect."), (2,"Both statements are independent.")]),
                  ("Speculation is defined as assuming something to be true based on inconclusive evidence.  Which of the following is the best example of speculation?", [(0,"Ama decides it will be appropriate to wear jeans to her new office on Friday after reading about 'Casual Fridays' in her employee handbook."),
(1,"Kofi spends thirty minutes sitting in traffic and wishes that he rode his motorbike instead of driving."),
(2,"After consulting several guidebooks and her travel agent, Julie feels confident that the hotel she has chosen is first rate."),
(3,"When Efia opens the door in tears, Theo guesses that she's had a death in her family.")]) ]
    qA = forms.IntegerField(widget=RadioSelect(choices=QUESTIONS[0][1]), label=QUESTIONS[0][0])
    qB = forms.IntegerField(widget=RadioSelect(choices=QUESTIONS[1][1]), label=QUESTIONS[1][0])
    qC = forms.IntegerField(widget=RadioSelect(choices=QUESTIONS[2][1]), label=QUESTIONS[2][0])
    qD = forms.IntegerField(widget=RadioSelect(choices=QUESTIONS[3][1]), label=QUESTIONS[3][0])
    qE = forms.IntegerField(widget=RadioSelect(choices=QUESTIONS[4][1]), label=QUESTIONS[4][0])
    qF = forms.IntegerField(widget=RadioSelect(choices=QUESTIONS[5][1]), label=QUESTIONS[5][0])
    qG = forms.IntegerField(widget=RadioSelect(choices=QUESTIONS[6][1]), label=QUESTIONS[6][0])
    qH = forms.IntegerField(widget=RadioSelect(choices=QUESTIONS[7][1]), label=QUESTIONS[7][0])
    qI = forms.IntegerField(widget=RadioSelect(choices=QUESTIONS[8][1]), label=QUESTIONS[8][0])
    qJ = forms.IntegerField(widget=RadioSelect(choices=QUESTIONS[9][1]), label=QUESTIONS[9][0])
    def score(self):
        scores = [  (1, self.cleaned_data['qA']),
                    (2, self.cleaned_data['qB']),
                    (3, self.cleaned_data['qC']),
                    (4, self.cleaned_data['qD']),
                    (5, self.cleaned_data['qE']),
                    (6, self.cleaned_data['qF']),
                    (7, self.cleaned_data['qG']),
                    (8, self.cleaned_data['qH']),
                    (9, self.cleaned_data['qI']),
                    (9, self.cleaned_data['qJ']) ]
        scores.sort(key=lambda x: x[1])
        return int("".join(map(lambda x: str(x[0]), scores)))
