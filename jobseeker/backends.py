from common.models import Jobseeker, Jobseeker_auth

class JobseekerAuthBackend:
    def authenticate(self, username=None, password=None, **kwargs):
        try:
            ja = Jobseeker_auth.objects.get(auth_code=username)
            return ja.user
        except:
            return None

    def get_user(self, user_id):
        try:
            return Jobseeker.objects.get(pk=user_id)
        except Jobseeker.DoesNotExist:
            return None
