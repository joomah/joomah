from django import template
from common.models import EducationEvent, JobListing, CareerEvent


register = template.Library()

def a(val):
    if not val: return 'Undisclosed'
    return  [ x[1] for x in CareerEvent.SALARIES if x[1] != 'Undisclosed' ][val]

def b(val):
    if not val: return 'Full Time'
    return [ x[1] for x in JobListing.COMMITMENTS ][val]

def edu(val):
    if not val: return 'None'
    return [ x[1] for x in EducationEvent.LEVELS if x[1] != 'Degree' ][val]


def to_ssl(val):
    return val.replace('http:','https:')

def columns(thelist, n):
    try:
        n = int(n)
        thelist = list(thelist)
    except (ValueError, TypeError):
        return [thelist]
    list_len = len(thelist)
    split = list_len // n
    if list_len % n != 0:
        split += 1
    return [thelist[i::split] for i in range(split)]


register.filter('a',a)
register.filter('b',b)
register.filter('edu',edu)
register.filter('to_ssl',to_ssl)
register.filter('columns', columns)
