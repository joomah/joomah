from __future__ import with_statement
from fabric.api import local, run, cd
from fabric.context_managers import prefix
from fabric.operations import sudo
from fabric.state import env

CHECKOUT_DIR = '/tmp'
DEPLOY_DIR = '/srv'
VENV_DIR = '/srv/venv'


def test():
    env.user = 'admin'
    env.key_filename = '~/.ssh/id_rsa.pub'
    env.hosts = ['23.92.21.85']
    env.root_dir = 'test.joomah.com'
    env.local_settings = 'local_settings.py.test'
    env.wsgi = 'django.wsgi.test'


def live():
    env.user = 'admin'
    env.key_filename = '~/.ssh/id_rsa.pub'
    env.hosts = ['23.92.21.85']
    env.root_dir = 'joomah.com'
    env.local_settings = 'local_settings.py.live'
    env.wsgi = 'django.wsgi.live'

def checkout(branch):
    with cd(CHECKOUT_DIR):
        run("rm -rf joomah")
        run("git clone https://bitbucket.org/joomah/joomah.git")
    project_root = "%s/joomah" % CHECKOUT_DIR
    with cd(project_root):
        run("git checkout %s" % branch)


def provision():
    deploy_dir = "%s/%s/" % (DEPLOY_DIR, env.root_dir)
    with cd(deploy_dir):
        run("rm -rf *")
    with cd(CHECKOUT_DIR):
        sudo("cp -r joomah/* %s" % deploy_dir)
    with cd(deploy_dir):
        sudo("mv jobseeker/%s jobseeker/local_settings.py" % env.local_settings)
        sudo("mv jobseeker/%s jobseeker/django.wsgi" % env.wsgi)
        sudo("mv employer/%s employer/local_settings.py" % env.local_settings)
        sudo("mv employer/%s employer/django.wsgi" % env.wsgi)
        venv = "source %s/%s/bin/activate" % (VENV_DIR, env.root_dir)
        with prefix(venv):
            sudo("pip install -r requirements.txt")
            sudo("python manage-jobseeker.py migrate")
            sudo("python manage-jobseeker.py loaddata all")
        sudo("chown -R www-data %s" % deploy_dir)

def restart():
    sudo("/usr/sbin/service apache2 restart")


def deploy(branch):
    checkout(branch)
    provision()
    restart()
