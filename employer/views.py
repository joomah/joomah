from datetime import datetime
import jobseeker.settings
import django.forms
from django.shortcuts import render_to_response, render, redirect
from django.shortcuts import get_object_or_404
from django.http import Http404, HttpResponse, HttpResponseRedirect, HttpResponseBadRequest, HttpResponseNotAllowed, HttpResponseForbidden
from django.core.exceptions import ObjectDoesNotExist
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.core.context_processors import csrf
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Q
import json
from django.core.mail import send_mail
from django.core.mail import EmailMultiAlternatives
from employer.forms import JobListingForm, RegistrationForm, EditEmployerForm, ChangePasswordForm
from common.models import Jobseeker, HardClonedSeeker, Employer, EducationEvent, CareerEvent, JobListing, Application, SavedSearch, Application, Industry, Location, Language
from django.db.models import Q, Sum
from django.utils.timezone import utc
from datetime import datetime, timedelta
from twilio.rest import TwilioRestClient
import re
import random
import operator
import stripe
import urllib, urllib.request
from jobseeker.views import render_to_pdf

html_head = ("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>\r\n"
             "<html xmlns='http://www.w3.org/1999/xhtml'>\r\n"
             "<head>\r\n"
             "<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />\r\n"
             "<meta name='description' content=\"J&oacute;&ograve;Mah, the place to find jobs near you. Join Top Jobs in Ghana - Ghana's Leading Job Site.\">\r\n"
             "<title>J&oacute;&ograve;Mah: Applicant Screening Made Easy</title>\r\n"
             "<link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>\r\n"
             "</head>\r\n"
             "<body style='background: #ECF0F1; font-size: 12px;'>\r\n"
             "<div style='width: 60%; min-width: 800px; max-width: 800px; margin-left: auto; margin-right: auto; color: gray; margin-top: 40px;'>\r\n")
html_foot = ("  <div style='color: gray;width: 60%; min-width: 600px; margin-left: auto; margin-right: auto; margin-top: 100px; text-align: center;'>\r\n"
             "    <div>\r\n"
             "      <span style='text-decoration: none; color: inherit; margin: 10px;'>Tell you friends and colleagues about us on "
             "      <a href='https://www.facebook.com/pages/J%C3%B3%C3%B2Mah/827539130595240' style='text-decoration: none; color: #2FA8D1;'>Facebook</a> or "
             "      <a href='https://twitter.com/JoomahAfrica' style='text-decoration: none; color: #2FA8D1;'>Twitter</a></span>\r\n"
             "    </div>\r\n"
             "    <div>\r\n"
             "      <a href='http://jobs.joomah.com/terms' style='text-decoration: none; color: #2FA8D1; margin: 10px;'>Privacy Policy</a>\r\n"
             "      <a href='mailto:info@joomah.com' style='text-decoration: none; color: #2FA8D1; margin: 10px'>Contact us</a>\r\n"
             "    </div>\r\n"
             "  </div>\r\n"
             "</div>\r\n"
             "</body>\r\n"
             "</html>\r\n")

def get(a, i, d):
    try:
        return a[i]
    except:
        return d

def home(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/start/')
    form = RegistrationForm() # unbound form
    return render(request, 'signin.html', { 'retries': request.GET.get('retries'), 'form':form })

def mobile_home(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/start/')
    return render(request, 'mobile_homepage.html')

def login(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/start/')
    return render(request, 'mobile_login.html')

@login_required
def cover(request):
    return render(request, 'cover.html')

@login_required    
def application_manager(request, listingId):
    title = 'Your Job List'
    listing = JobListing.objects.get(id=listingId)
    if listing.owner != request.user:
        return HttpResponseForbidden()
    if listing.end_date <= datetime.utcnow().replace(tzinfo=utc).date():
        raise Http404
    return render(request, 'application-manager.html', {'job': listing, 'title': title})
    #return render(request, 'application-manager.html', { 'job': listing, 'form': JobListingForm(instance=listing) })

def score_for_job(job, user, non_applicant=False):
    i = 0
    i += user.get_total_experience()
    i += 10 * (user.get_industry_experience(job.industry))
    if user.get_industry_experience(job.industry) < 12 * job.experience:
        i -= 50
    max_edu = user.get_max_education()
    max_edu = int(max_edu) if max_edu else 0
    i += 30 * max_edu
    if max_edu < job.min_degree:
        i -= 400
    i += 10 * len(set(user.skills.all()).intersection(job.requirements.all()))
    if not user.resume:
        i -= 1000
    if non_applicant: # let's remove the over qualified
        i -= max(0,user.get_industry_experience(job.industry) - 12 * (job.experience + 3))
    return -i

def max_score_for_job(job):
    i = 0
    i += 120 * job.experience
    i += 30 * job.min_degree
    i += 10 * len(job.requirements.all())
    return i


def compare(job):
    def compare_users(app):
        return score_for_job(job, app.applicant)
    return compare_users

def compare_all(job):
    def compare_users(user):
        return score_for_job(job, user, non_applicant=True)
    return compare_users

@login_required
def get_users(request, listingId):
    listing = JobListing.objects.get(id=listingId)
    if listing.owner != request.user:
        return HttpResponseForbidden()
    all_users = Jobseeker.objects.filter(industries__in=[listing.industry])
    all_users = sorted(all_users, key=compare_all(listing))
    ret = []
    max_score = max_score_for_job(listing)
    for user in all_users:
        rating = None
        try:
            rating = Application.objects.get(applicant=user, job=listing).rating
        except ObjectDoesNotExist:
            pass
        user_score = score_for_job(listing, user)
        if max_score == 0:
            match = 100
        else:
            match = -1
            if user_score > 0:
                match = 0
            else:
                user_score = user_score * -1
                match = int(100 - 100.0 * abs(max_score - user_score) / max_score)

        ret.append({
            'name': user.get_full_name(),
            'doc1': str(user.resume),
            'profile_photo': str(user.profile_photo),
            'id': user.id,
            'user_id': user.id,
            'rating': rating,
            'pct_match': match
            })
    return HttpResponse(json.dumps({'applicants': ret}), content_type='application/json')

@login_required
def add_applicant(request, listingId, userId):
    listing = JobListing.objects.get(id=listingId)
    if listing.owner != request.user:
        return HttpResponseForbidden()
    try:
        app = Application.objects.get(applicant__id=userId, job=listing)
    except ObjectDoesNotExist:
        app = Application.objects.create(forced=True, applicant=Jobseeker.objects.get(id=userId),job=listing).save()
    return HttpResponse(json.dumps({ 'id': app.id }), content_type='application/json')

@login_required
def get_applicants(request, listingId):
    listing = JobListing.objects.get(id=listingId)
    if listing.owner != request.user:
        return HttpResponseForbidden()
    applications = Application.objects.filter(job=listingId)
    if not listing.random_ranking:
        applications = sorted(applications, key=compare(listing))
    applicants = []
    max_score = max_score_for_job(listing)
    for app in applications:
        user_score = score_for_job(listing, app.applicant)
        if max_score == 0:
            match = 100
        else:
            match = -1
            if user_score > 0:
                match = 0
            else:
                user_score = user_score * -1
                match = int(100 - 100.0 * abs(max_score - user_score) / max_score)
        applicants.append({
            'name': app.applicant.get_full_name(),
            'doc1': str(app.applicant.resume),
            'letter': app.cover_letter,
            'profile_photo': str(app.applicant.profile_photo),
            'rating': app.rating,
            'id': app.id,
            'user_id': app.applicant.id,
            'pct_match': match
            })
    if listing.random_ranking: random.shuffle(applicants)
    return HttpResponse(json.dumps({'applicants': applicants}), content_type='application/json')

@login_required
def contact(request):
    if request.method == 'POST':
        if request.user.next_mail_date and request.user.next_mail_date > datetime.utcnow().replace(tzinfo=utc):
            return HttpResponseForbidden("<html><body><p>Must wait before sending more messages.</p></body></html>")
        mail_to = list(map(lambda x: x.email, Jobseeker.objects.filter(id__in=request.POST.getlist('users[]'))))
        message = ( "You have received a message from " + request.user.name + ":\n"
                    "" + request.POST['message'] + "\n\nPlease do not reply to info@joomah.com"
                    ", instead you can reply to " + request.user.name + " directly at " + request.user.email + "." )
        msg = EmailMultiAlternatives("Message from " + request.user.name + ": " + request.POST['subject'], message, 'info@joomah.com', mail_to)
        msg.headers =  { 'Reply-To': request.user.email }
        msg.send()
        request.user.next_mail_date = datetime.utcnow().replace(tzinfo=utc) + timedelta(seconds=2*len(mail_to))
        request.user.save()
    return HttpResponse('{}', content_type='application/json')

@login_required
def rate_applicant(request, listingId, applicationId, newRating):
    listing = JobListing.objects.get(id=listingId)
    if listing.owner != request.user:
        return HttpResponseForbidden()
    app = Application.objects.get(id=applicationId)
    app.rating = newRating
    app.save()
    deep_clone = HardClonedSeeker.objects.create()
    deep_clone.set_seeker(app.applicant)
    return HttpResponse('{}', content_type='application/json')

@login_required
def extend_deadline(request, listingId, days):
    listing = JobListing.objects.get(id=listingId)
    if listing.owner != request.user:
        return HttpResponseForbidden()
    listing.end_date = listing.end_date + timedelta(days=int(days))
    listing.save()
    return HttpResponse('{}',content_type='application/json')

@login_required
def edit_job(request, listingId):
    listing = JobListing.objects.get(id=listingId)
    if listing.owner != request.user:
        return HttpResponseForbidden()
    form = JobListingForm(request.POST, instance=listing)
    if form.is_valid(): # All validation rules pass
        form.save(commit=True)
        return HttpResponse('{}', content_type='application/json')
    return HttpResponseBadRequest(json.dumps({ 'errors': form.errors }), content_type='application/json')

@login_required
def reopen(request, listingId):
    listing = JobListing.objects.get(id=listingId)
    if listing.owner != request.user:
        return HttpResponseForbidden()
    listing.end_date = datetime.utcnow().replace(tzinfo=utc) + timedelta(days=7)
    listing.save()
    return HttpResponse('{}',content_type='application/json')


@login_required
def close_job(request, listingId):
    listing = JobListing.objects.get(id=listingId)
    if listing.owner != request.user:
        return HttpResponseForbidden()
    listing.end_date = datetime.utcnow().replace(tzinfo=utc)
    listing.save()
    return HttpResponse('{}', content_type='application/json')

@login_required
def post_job(request):
    if request.method == 'POST': # Form has been submitted
        form = JobListingForm(request.POST)
        if form.is_valid(): # All validation rules pass
            job_listing = form.save(commit=False)
            job_listing.owner = request.user
            job_listing.views = 0
            if random.random() > 0.7:
                job_listing.random_ranking = True
            job_listing.save()
            return HttpResponse(json.dumps({ 'id': job_listing.id }), content_type='application/json')
        return HttpResponseBadRequest(json.dumps({ 'errors': form.errors }), content_type='application/json')
    return HttpResponseNotAllowed(['POST'])

#Experimenting with the new job posting page
def create_link(request):
    title = 'Create A New Job Link'
    form = JobListingForm(request.POST)
    return render(request, 'new-job-link.html', { 'form': JobListingForm, 'title': title, })


@login_required
def search_by_name(request):
    name = request.GET.get('name','')
    names = name.split()
    if len(name) < 3:
        return render(request, 'search_results.html', { 'error': 'Please enter in a name to search, it must be at least 3 characters.' })
    users = Jobseeker.objects.filter(Q(first_name__in=names) | Q(last_name__in=names))
    ret = []
    for user in users:
        try:
                info = {}
                info['name'] = user.get_full_name()
                info['id'] = user.id
                bestEvent = user.get_max_education()
                if bestEvent is not None: info['highest_education'] = unicode(bestEvent)
                info['title'] = user.occupation()
                info['last_job'] = user.get_last_job()
                info['resume'] = user.resume
                info['total_experience'] = int(user.get_total_experience()/12)
                info['industries'] = user.industries
                ret.append(info)
        except:
            pass
    SavedSearch.objects.create(query=json.dumps({'owner': 'employer', 'parameters': {'name': name}})).save()
    return render(request, 'search_results.html', { 'results': ret, 'form' : JobListingForm, 'query': 'Searched for jobseekers with name "' + name + '"'})

@login_required
def open_jobs(request):
    title = 'Your Job List'
    ret = []
    jobs = request.user.joblisting_set.filter(end_date__gt=datetime.utcnow().replace(tzinfo=utc).date())
    for j in jobs:
        apps = Application.objects.filter(job=j.id)
        ret.append([j,len(apps),len(apps.filter(rating__gte=5))])
    apps = []
    return render(request, 'application-manager.html', { 'current': True, 'jobs': ret, 'apps': apps, 'title': title })

@login_required
def search(request):
    # add search by degree, qualification, location
    try:
        industry = Industry.objects.get(id=int(request.GET['industry']))
    except:
        industry = None
    if industry:
        users = Jobseeker.objects.filter(industries__in=[industry])
    else:
        users = Jobseeker.objects.all()
    query = {}
    ret = []
    for user in users:
        edu = user.get_max_education()
        if (not edu and int(request.GET.get('min_degree',0)) == 0) or (edu and int(edu) >= int(request.GET.get('min_degree',0))):
            if user.get_total_experience() >= 12 * int(request.GET.get('experience',0)):
                info = {}
                info['name'] = user.get_full_name()
                info['id'] = user.id
                info['highest_education'] = edu
                occ = user.occupation()
                if occ:
                    info['title'] = occ.job_title
                    info['last_job'] = occ.employer_name
                info['cv'] = user.resume
                info['industries'] = user.industries.all()
                info['total_experience'] = int(user.get_total_experience() / 12) 
                ret.append(info)
    degree_name = [v for i, v in EducationEvent.LEVELS if i == request.GET.get('min_degree',0)]
    if len(degree_name) > 0:
        degree_name = degree_name[0]
    else:
        degree_name = "any"
    query = {   'owner': 'employer',
                'parameters': {
                    'industry': str(industry) if industry else '',
                    'min_degree': request.GET.get('min_degree',0),
                    'experience': request.GET.get('experience',0),
                }
            }
    SavedSearch.objects.create(query=json.dumps(query)).save()
    return render(request, 'search_results.html', {'results': ret, 'form' : JobListingForm(request.GET), 'query': "Searched for workers in " + (str(industry) if industry else 'any industry') + " with minimum degree " + degree_name + " with " + str(request.GET.get('experience','0')) + " years of experience." })

@login_required
def about(request):
    try:
        user = Jobseeker.objects.get(id=request.GET.get('user'))
    except ObjectDoesNotExist:
        raise Http404
    occupation = user.occupation()
    employer = ''
    if occupation:
        employer = occupation.employer_name
        occupation = occupation.job_title
    return render(request, 'about.html', { 'about_user': user,
        'occupation': occupation, 'employer': employer,
        'occupation_history': user.careerevent_set.all(),
        'education': user.educationevent_set.all(),
        'skills': user.skills.all(),
        'languages': user.languages.all(), 'industries': user.industries.all() })
    return render(request, 'about.html')

@login_required
def start(request):
    title = 'Home'
    #listing = JobListing.objects.get(id=listingId)
    curr_jobs = JobListing.objects.filter(
                                          owner=request.user, 
                                          start_date__lte=datetime.utcnow().replace(tzinfo=utc).date(),
                                          end_date__gte=datetime.utcnow().replace(tzinfo=utc).date()
                                         )
    num_curr_jobs = curr_jobs.count()
    num_views = curr_jobs.aggregate(Sum('views'))['views__sum']

    curr_apps = Application.objects.filter(
                                           job__owner=request.user,
                                           job__start_date__lte=datetime.utcnow().replace(tzinfo=utc).date(),
                                           job__end_date__gte=datetime.utcnow().replace(tzinfo=utc).date()
                                      )
    num_curr_apps = curr_apps.count()

    num_upcoming_deadlines = JobListing.objects.filter( 
                                                       owner=request.user,
                                                       end_date__gte=datetime.utcnow().replace(tzinfo=utc).date(),
                                                       end_date__lte=(datetime.utcnow().replace(tzinfo=utc) + timedelta(days=14)).date()
                                                      ).count()

    all_listings = JobListing.objects.filter(owner=request.user)
    edu_lvls = {}
    for e in EducationEvent.LEVELS: # fill dict with Education Values
        edu_lvls[int(e[0])] = 0
    del edu_lvls[0]

    for app in curr_apps:           # calculate number of each education level for all current apps
        edu_lvl = app.applicant.get_max_education()
        if edu_lvl and int(edu_lvl) > 0: edu_lvls[int(edu_lvl)] += 1

    #template assumers education level numbers be sorted in descending order
    sorted_edu_lvls = sorted(edu_lvls.items(), key=operator.itemgetter(1), reverse=True)

    if num_curr_apps != 0:      #calculate percent for each education level
        for i, (r,num) in enumerate(sorted_edu_lvls): sorted_edu_lvls[i] = (num, (num * 100.0) // float(num_curr_apps), EducationEvent.LEVELS[r+1][1])
    else:
        for i, (r,num) in enumerate(sorted_edu_lvls): sorted_edu_lvls[i] = (num, 0, EducationEvent.LEVELS[r+1][1])

    form = JobListingForm()
    user_country = get(request.user.locations.all(), 0, Location.objects.get(id=1))
    form.fields['location'].queryset = Location.objects.filter(country=user_country.country, city__isnull=False)
    form.fields['languages'].queryset = Language.objects.filter(Q(country=user_country) | Q(country__isnull=True))

    return render(request, 'start.html', { 'form': form,
                                           'title': title,
                                           'jobs_open': num_curr_jobs,
                                           'jl_views': num_views if num_views else 0,
                                           'num_apps': num_curr_apps,
                                           'num_upcoming_deadlines': num_upcoming_deadlines,
                                           'edu_lvls': sorted_edu_lvls,
                                           'country': str(get(request.user.locations.all(),0,'Ghana')),
                                           'jobs': curr_jobs,
                                           'current': True, })

@login_required
def settings(request):
    title = 'Account Settings'
    if request.method == 'POST':
        name = request.POST.get('name')
        pwd_form = ChangePasswordForm(request.POST, instance=request.user)
        form = EditEmployerForm(request.POST, request.FILES, instance=request.user)
        if name == 'password':
            if pwd_form.is_valid():
                pwd_form.save(commit=True)
                return HttpResponseRedirect('/start/')
            else: 
                return render(request, 'settings.html', {'form': EditEmployerForm(instance=request.user), 'pwd_form': pwd_form })
        else:
            if form.is_valid():
                form.save(commit=True)
                return HttpResponseRedirect('/start/')
            else: 
                return render(request, 'settings.html', {'form': form, 'pwd_form': ChangePasswordForm() })
    pwd_form = ChangePasswordForm()
    form = EditEmployerForm(instance=request.user)
    return render(request, 'settings.html', { 'form': form, 'pwd_form': pwd_form, 'title': title, })

@login_required
def history(request):
    title = 'History'
    ret = []
    jobs = request.user.joblisting_set.filter(end_date__lte=datetime.utcnow().replace(tzinfo=utc).date())
    for j in jobs:
        apps = Application.objects.filter(job=j.id)
        ret.append([j,len(apps),len(apps.filter(rating__gte=5))])
    return render(request, 'history.html', { 'current': False, 'jobs': ret, 'title':title, })

def verify(request, auth_code):
    try:
        u = Employer_auth.objects.get(auth_code=auth_code).user
    except:
        return HttpResponse(status=404)
    u.verified = True
    u.save()
    user = auth.authenticate(username=u.name, password=auth_code)
    auth.login(request, user)
    return HttpResponseRedirect('/start/')

def register(request):
    if request.method == 'POST': # Form has been submitted
        form = RegistrationForm(request.POST)
        if form.is_valid(): # All validation rules pass
            new_employer = form.save()
            user = auth.authenticate(username=request.POST['name'], password=request.POST['password'])
            auth.login(request, user)
            new_token = user.generate_token()
            subject="Welcome to JooMah"
            html_content = ("<div>\r\n"
                            "<div style='color: #0096C9; background: #FFFFFF; font-size: 3em; padding: 10px;'>J&oacute;&ograve;mah</div>\r\n"
                            "<div style='background: #0096C9; color: #FFFFFF; font-size: 2em; padding: 10px;'>Welcome!</div>\r\n"
			    "</div>\r\n"
			    "<div style='background: #FFFFFF; margin: 30px 60px; padding: 10px 20px;'>\r\n"
			    "<div style='color: #A5A5A5; font-size: 1.5em;'>\r\n"
			      "<div style='padding: 10px 0px'>Hello " + user.name + ",</div>\r\n"
			      "<div  style='padding: 10px 0px'> I really appreciate you joining us at J&oacute;&ograve;Mah, and I know you will love it when you see how easy it is to hire the right people using J&oacute;&ograve;Mah.</div>\r\n"
			      "<div style='padding: 10px 0px'> Take the first step towards finding your next great hire by jumping right in and verifying your account by clicking <a href='/verify/" + new_token.auth_code + "/'>here</a>.</div>\r\n"
			      "<div style='padding: 10px 0px'> We built J&oacute;&ograve;Mah to help businesses like yours grow, and I hope that we can achieve that for you.</div>\r\n"
                              "<div style='padding: 10px 0px'>J&oacute;&ograve;Mah takes care of all your hiring needs in 3 simple steps:\r\n"
                                "<div>\r\n"
                                  "<div>1. Post your job vacancy and J&oacute;&ograve;Mah pushes it out to multiple job sites to reach the best candidates wherever they are.</div>\r\n"
                                  "<div>2. J&oacute;&ograve;Mah shows you how well each candidate matches your hiring needs so you can easily identify the best.</div>\r\n"
                                  "<div>3. Easily manage, track and even invite candidates for interviews.</div>\r\n"
                                "</div>\r\n"
                              "</div>\r\n"
                                "<div style='padding: 10px 0px'>You get all this value for only $5 per job listing. Get started today for a 14 day free trial. </div>\r\n"
                                "<div style='padding: 10px 0px'> By the way, over the next couple of weeks, we will be sending you a few more emails to help you get maximum value from J&oacute;&ograve;Mah.</div>\r\n"
                                "<div style='padding: 10px 0px'> We'll be sharing some tips, checking in with you and showing you how some of our customers use J&oacute;&ograve;Mah to grow their businesses.</div>\r\n"
                                "<div style='padding:0px;'>Thanks,</div>\r\n"
                                "<div style='padding:0px;'>Kwaku Akoi</div>\r\n"
			        "<div style='padding:0px;'>CEO, J&oacute;&ograve;Mah</div>\r\n"
                                "<div style='padding:0px;'>info@joomah.com</div>\r\n"
                                "<div style='padding:0px;'>+1-901-942-8008</div>\r\n"
                              "</div>\r\n")
            html_message = html_head + html_content + html_foot
            message="Welcome to Joomah.com! Take the first step towards finding your next great hire by jumping right in and using JooMah to track your job openings.  We've also put together a short demo video to help you get started with your JooMah account.  Watch the video at https://youtu.be/DIbAKz2sM_0.  Have any questions or feedback? You can always send us an email at info@joomah.com.  We are here to help and would love to hear from you!"
            message+="\nThanks,"+"\n"+"The JooMah Team."
            from_email, to = 'info@joomah.com', request.POST.get('email')
            msg = EmailMultiAlternatives(subject, message, from_email, [to])
            msg.attach_alternative(html_message, "text/html")
            msg.send()
            return HttpResponseRedirect('/cover/')
    else:
        form = RegistrationForm() # unbound form
    return render(request, "register.html", {'form': form })

def terms(request):
    return render(request, "terms.html")

def faq(request):
    return render(request, "faq.html")

def about_joomah(request):
    return render(request, "aboutjoomah.html")

def pricing(request):
    return render(request, "pricing.html")

def contact(request):
    title = 'We love to hear from you'
    return render(request, "contact_us.html", {'title': title,})

def tips(request):
    title = 'Everything you need to know about using ATS'
    return render(request, "tips.html", {'title': title,})

@login_required
def payment_confirmation(request):
    if request.GET.get('tx'): # this is a PayPal confirmation
        tx = request.GET.get('tx')
        post_data = [('cmd','_notify-synch'),('tx',tx),('at','_gNILgY2Bj20Ju5-gUprNmW5YlaqoD3fg7H4DFAPdlGKGisez1Su0NyU6Ca'),]
        result = urllib2.urlopen("https://www.paypal.com/cgi-bin/webscr", urllib.urlencode(post_data))
        data = result.read()
        paypal_info = dict(filter(lambda x: len(x)>1, map(lambda x: x.split("="), data.split("\n"))))
        if paypal_info['item_name'] == 'Single+Post':
            request.user.money_in_account += 500
            request.user.save()
        else: # monthly
            request.user.subscription_type = 'basic'
            start = datetime.now()
            if start.month < 12:
                request.user.subscription_end = datetime(start.year, start.month+1, start.day)
            else:
                request.user.subscription_end = datetime(start.year+1, 1, start.day)
            request.user.save()
    return render(request, "payment_confirmation.html")

@login_required
def payment_submission(request):
    stripe.api_key = "sk_live_4KrybT3oziN5bVJ3htir5i40"
    if request.method == 'POST':
        stripeToken = request.POST.get('stripeToken')
        item = request.POST.get('plan','0')
        if stripeToken:
            if item == '0':
                charge = stripe.Charge.create(amount=500, currency="usd", source=stripeToken, receipt_email=request.POST.get('email'))
            elif item == '1':
                customer = stripe.Customer.create(source=stripeToken, plan="monthly", email=request.POST.get('email'))
                request.user.stripe_customer_id = customer.id
                request.user.save()
            elif item == '2':
                customer = stripe.Customer.create(source=stripeToken, plan="quarterly", email=request.POST.get('email'))
                request.user.stripe_customer_id = customer.id
                request.user.save()
            return HttpResponseRedirect('/payment-confirmation/')
    return render(request, "payment_submission.html", { 'item': request.GET.get('item', request.POST.get('item')) })

@login_required
def resume(request, jId):
    user = Jobseeker.objects.get(id=jId)
    return render_to_pdf('desktop/resume.html', {
        'user': user,
        'occupation': user.occupation(),
        'selected': 'about'  })

def recover(request, key):
    try:
        ja = Employer_auth.objects.get(auth_code=key)
    except ObjectDoesNotExist:
        raise Http404
    if request.method == 'POST':
        pwd = request.POST.get('password')
        pwd1 = request.POST.get('password1')
        if not pwd or len(pwd) < 8: return render(request, 'recover.html', {'errors': "Password must be at least 8 characters." })
        if not pwd1 or pwd != pwd1: return render(request, 'recover.html', {'errors': "Passwords do not match." })
        ja.verified = True # if they got this email we know their email address is correct
        ja.user.set_password(pwd)
        ja.user.verified = True
        ja.user.save()
        user = auth.authenticate(username=ja.auth_code)
        auth.login(request, user)
        ja.delete()
        return HttpResponseRedirect('/start/')
    return render(request, 'recover.html')

def forgot_password(request):
    if request.method == 'POST':
        try:
            user = Employer.objects.get(name=request.POST.get('name'))
        except ObjectDoesNotExist:
            return render(request, 'forgot_password.html', { 'errors': 'Email address is not associated with an account.' })
        new_token = user.generate_token()
        subject="JooMah Account Recovery"
        html_content = ("<div>\r\n"
                        "  <div style='color: #0096C9; background: #FFFFFF; font-size: 3em; padding: 10px;'>J&oacute;&ograve;mah</div>\r\n"
                        "    <div style='background: #0096C9; color: #FFFFFF; font-size: 2em; padding: 10px;'>Account Recovery"
                        "    </div>                               "
                        "</div>\r\n"
                        "<div style='background: #FFFFFF; margin: 30px 60px; padding: 10px;'>\r\n"
                        "  <div style='color: #A5A5A5; font-size: 1.5em;'>To recover your account please follow the link below. If you did not request account recovery you can safely ignore this message.</div>\r\n"
                        "</div>\r\n"
                        "<div style='background: #888888; width: 200px; margin-left: auto; margin-right: auto; color: #FFFFFF; text-align: center; font-size: 3em;          padding: 3px; margin-top: 20px;'>\r\n"
                        "  <a href='https://joomah.com/recover/"+ new_token.auth_code+"' style='text-decoration: none; color: inherit;'>Recover</a>\r\n"
                        "</div>\r\n")
        html_message = html_head + html_content + html_foot
        message="To recover your account please follow the link to http://jobs.joomah.com/recover/" + new_token.auth_code
        message+="\nIf you did not request account recovery you can safely ignore this message."
        message+="\nThanks,"+"\n"+"The JooMah Team."
        from_email, to = 'info@joomah.com', request.POST.get('email')
        msg = EmailMultiAlternatives(subject, message, from_email, [to])
        msg.attach_alternative(html_message, "text/html")
        msg.send()
        return render(request, 'forgot_password.html', { 'errors': 'Password reset link sent to the email address!' })
    return render(request, 'forgot_password.html')

@csrf_exempt
def invoice_paid(request):
    event_id = json.loads(request.body)
    event_id = event_id['id']
    stripe.api_key = "sk_live_4KrybT3oziN5bVJ3htir5i40"
    event = stripe.Event.retrieve(event_id)
    event_data = event['data']['object']
    if event_data.get('paid'):
        employer = Employer.objects.get(stripe_customer_id=event_data['customer'])
        item = event_data['lines']['data'][0]
        if item['plan']['id'] == 'basic':
            employer.subscription_type = 'basic'
            start = datetime.utcfromtimestamp(event['created'])
            if start.month < 12:
                account.subscription_end = datetime(start.year, start.month+1, start.day)
            else:
                account.subscription_end = datetime(start.year+1, 1, start.day)
        else:
            employer.money_in_account += amount
        employer.save()
    return HttpResponse()

@login_required
def change_credit(request):
    stripe.api_key = "sk_live_4KrybT3oziN5bVJ3htir5i40"
    token = request.POST.get('stripeToken')
    customer = stripe.Customer.retrieve(request.user.stripe_customer_id)
    card = customer.cards.create(card=token)
    card.save()
    customer.default_card = card.id
    customer.save()
    return HttpResponse()

@login_required
def applicant_tracking(request, listingId):
    listing = JobListing.objects.get(id=listingId)
    title = listing.title + 'Applicant Tracking'
    if listing.owner != request.user:
        return HttpResponseForbidden()
    all_users = map(lambda x: x.applicant, Application.objects.filter(job=listing))
    all_matches = dict()
    max_score = max_score_for_job(listing)
    for user in all_users:
        rating = None
        try:
            rating = Application.objects.get(applicant=user, job=listing).rating
        except ObjectDoesNotExist:
            pass
        user_score = score_for_job(listing, user)
        if max_score == 0:
            match = 100
        else:
            match = -1
            if user_score > 0:
                match = 0
            else:
                user_score = user_score * -1
                match = int(100 - 100.0 * abs(max_score - user_score) / max_score)
        all_matches[user.id] = match
    all_users = sorted(all_users, key=compare_all(listing))
    shortlist = map(lambda x: x.applicant, filter(lambda x: x.shortlist, Application.objects.filter(job=listing)))
    reject = map(lambda x: x.applicant, filter(lambda x: x.reject, Application.objects.filter(job=listing)))
    users = filter(lambda x: not (x in shortlist or x in reject), all_users)

    return render(request, 'applicant_tracking.html', {'applicants': users, 'title': title, 'shortlist': shortlist, 'reject': reject, 'listing': listing, 'matches' : all_matches })

@login_required
def shortlist(request, listingId):
    listing = JobListing.objects.get(id=listingId)
    if listing.owner != request.user:
        return HttpResponseForbidden()
    for appId in request.POST.getlist('on_shortlist[]'):
        a = Application.objects.get(applicant__id=appId, job=listing)
        a.shortlist = True
        a.reject = False
        a.save()
    for appId in request.POST.getlist('off_shortlist[]'):
        a = Application.objects.get(applicant__id=appId, job=listing)
        a.shortlist = False
        a.reject = False
        a.save()
    return HttpResponse()

@login_required
def shortlist(request, listingId):
    listing = JobListing.objects.get(id=listingId)
    if listing.owner != request.user:
        return HttpResponseForbidden()
    for appId in request.POST.getlist('on_shortlist[]'):
        a = Application.objects.get(applicant__id=appId, job=listing)
        a.reject = True
        a.shortlist = False
        a.save()
    for appId in request.POST.getlist('off_shortlist[]'):
        a = Application.objects.get(applicant__id=appId, job=listing)
        a.reject = False
        a.shortlist = False
        a.save()
    return HttpResponse()

def product(request):
    return render(request, 'product.html')
