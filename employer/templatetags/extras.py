from django.template import Library
from datetime import datetime

register = Library()

@register.filter
def get_years(start_year):
    return map(lambda x: str(x), range(start_year, datetime.now().year))

@register.filter
def check_empty(val):
    if val: return val
    return ''

@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)

