from django import forms
from django.forms import ModelForm, ValidationError
from django.forms.widgets import Select
from django.forms.extras.widgets import SelectDateWidget
from common.models import JobListing, Employer, Location, Industry
from PIL import Image
from datetime import datetime, timedelta

class JobListingForm(ModelForm):
    class Meta:
        model = JobListing
        exclude = ('owner','random_ranking','calendar','views',)
        widgets = {
            'end_date': SelectDateWidget(years=[x for x in range(datetime.now().year, datetime.now().year+3)]),
        }
    def __init__(self, *args, **kwargs):    
        super(JobListingForm, self).__init__(*args, **kwargs)
        self.fields['industry'].empty_label = 'Industry'
    def clean_end_date(self):
        end_date = self.cleaned_data.get('end_date')
        if end_date < datetime.now().date() + timedelta(days=13):
            raise ValidationError("Deadline must be at least two weeks from today.")
        return end_date


class RegistrationForm(ModelForm):
    password1 = forms.CharField(widget=forms.PasswordInput)
    class Meta:
        model = Employer
        fields = ('name', 'email', 'phone_number', 'industries', 'locations', 'primary_address', 'password', 'password1', 'category')
        widgets = { 'password': forms.PasswordInput() }

    def __init__(self, *args, **kwargs):
        super(RegistrationForm, self).__init__(*args, **kwargs)
        t = {'name':['Company Name', 'full'],'email':['Email', 'email'], 'phone_number':['Phone Number', 'full'], 'industries':['Industries', 'select'], 'locations':['Locations', 'select'], 'primary_address':['Address', 'full'], 'password':['Password', 'half'], 'password1':['Repeat Password', 'half'], 'category':['Category', 'dropdown']}
        for field in self.fields:
            self.fields[field].label = ''
            self.fields[field].widget.attrs\
            .update({'placeholder':str(t[field][0]),
                'class': str(t[field][1])
            })
        self.fields['category'].label = ''
        self.fields['locations'].label = ''
        self.fields['industries'].label = ''
        self.fields['locations'].help_text = ''
        self.fields['industries'].help_text = ''

    def clean(self):
        cleaned_data = super(RegistrationForm, self).clean()
        password = self.cleaned_data.get('password')
        password1 = self.cleaned_data.get('password1')
        if password and password1:
            if password != password1:
                raise ValidationError("Passwords do not match.")
            if len(password) < 8:
                raise ValidationError("Password must be at least 8 characters.")
        else:
            raise ValidationError("Please enter a password.")
        return cleaned_data

    def save(self, commit=True):
        emp = super(RegistrationForm, self).save(commit=True)
        emp.set_password(self.cleaned_data['password'])
        emp.is_active = True
        emp.save()
        return emp

class EditEmployerForm(ModelForm):
    address = forms.CharField(required=False)
    class Meta:
        model = Employer
        fields = ('email', 'phone_number', 'logo','number_of_employees','description','industries','primary_address','locations','description','year_founded', 'hire_in_year', 'prefered_skills', 'prefered_languages', 'interns', 'interns_in_year')
        widgets = { 'year_founded': Select(choices=[(x,x) for x in range(datetime.now().year,datetime.now().year - 200,-1)])}
    def clean_logo(self):
        image = self.cleaned_data.get('logo', None)
        if not image: return None
        try:
            size = 200, 200
            im = Image.open(image)
            im.thumbnail(size, Image.ANTIALIAS)
            image.seek(0)
            im.save(image)
            image.close()
        except (OSError, IOError): pass
        except: pass
        return image
    def save(self, commit=True):
        emp = super(EditEmployerForm, self).save(commit=commit)
        try:
            r, g, b, a = Image.open(emp.logo).convert('RGBA').getpixel((1,1))
            emp.header_color = "#" + "{:02x}{:02x}{:02x}".format(r,g,b)
            if a < 0.1: emp.header_color = "#fff"
        except: pass
        if commit: emp.save()
        return emp

class ChangePasswordForm(forms.Form):
    password = forms.CharField(widget=forms.PasswordInput, label="Change Password")
    password1 = forms.CharField(widget=forms.PasswordInput, label="Verify Password")
    def __init__(self, *args, **kwargs):
        self.instance = kwargs.pop('instance',None)
        super(ChangePasswordForm, self).__init__(*args, **kwargs)
    def clean_password1(self):
        password = self.cleaned_data.get('password','')
        password1 = self.cleaned_data.get('password1','')
        if password != password1:
            raise ValidationError("Passwords do not match.")
        if len(password) < 8:
            raise ValidationError("Password must be at least 8 characters.")
        return password1
    def save(self, commit=True):
        self.instance.set_password(self.cleaned_data['password'])
        if commit: self.instance.save()

