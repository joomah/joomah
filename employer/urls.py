from django.conf.urls import patterns, include, url
from django.views.generic import RedirectView
import common.urls
import employer.settings
from employer import views
from common.models import Employer, JobListing, Application, Jobseeker

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.site.register(Employer)
admin.site.register(Application)
admin.site.register(Jobseeker)
admin.autodiscover()


urlpatterns = patterns('',
    url(r'^$', 'employer.views.home'),
    url(r'^mobile/$', 'employer.views.mobile_home'),
    url(r'^mobile/login/$', 'employer.views.login'),
    url(r'^login/$', 'employer.views.home'),
    url(r'^register/$', 'employer.views.register'),
    url(r'^start/$', 'employer.views.start'),
    url(r'^create_link/$', 'employer.views.create_link'),
    url(r'^history/$', 'employer.views.history'),
    url(r'^jobopening/$', 'employer.views.post_job'),
    url(r'^settings/$', 'employer.views.settings'),
    url(r'^searchusersname/$', 'employer.views.search_by_name'),
    url(r'^search/$', 'employer.views.search'),
    url(r'^application-manager/$', 'employer.views.open_jobs'),
    url(r'^application-manager/([0-9]+)/$', 'employer.views.application_manager'),
    url(r'^application-manager/([0-9]+)/applicants/$', 'employer.views.get_applicants'),
    url(r'^application-manager/([0-9]+)/applicants/([0-9]+)/rate/([0-5])$', 'employer.views.rate_applicant'),
    url(r'^application-manager/([0-9]+)/extend/([0-9]+)/$', 'employer.views.extend_deadline'),
    url(r'^application-manager/([0-9]+)/close/$', 'employer.views.close_job'),
    url(r'^application-manager/([0-9]+)/candidates/$', 'employer.views.get_users'),
    url(r'^application-manager/([0-9]+)/candidates/add/([0-9]+)/$', 'employer.views.add_applicant'),
    url(r'^application-manager/([0-9]+)/edit', 'employer.views.edit_job'),
    url(r'^application-manager/contact/$', 'employer.views.contact'),
    url(r'^jobs/([0-9]+)/reopen/$', 'employer.views.reopen'),
    url(r'^favicon\.ico$', RedirectView.as_view(url='/static/img/favicon.ico')),
    url(r'^terms/$', 'employer.views.terms'),
    url(r'^faq/$', 'employer.views.faq'),
    url(r'^tips/$', 'employer.views.tips'),
    url(r'^about/$', 'employer.views.about'),
    url(r'^cover/$', 'employer.views.cover'),
    url(r'^aboutjoomah/$', 'employer.views.about_joomah'),
    url(r'^pricing/$', 'employer.views.pricing'),
    url(r'^product/$', 'employer.views.product'),
    url(r'^payment-confirmation/$', 'employer.views.payment_confirmation'),
    url(r'^payment-submission/$', 'employer.views.payment_submission'),
    url(r'^contact/$', 'employer.views.contact'),
    url(r'^resume/([0-9]+)/$', 'employer.views.resume'),
    url(r'^recover/',  'employer.views.forgot_password'),
    url(r'^recover/(.+)/',  'employer.views.recover'),
    url(r'^invoice/success/$', 'employer.views.invoice_paid'),
    url(r'^credit/change/$', 'employer.views.change_credit'),
    url(r'^verify/([0-9]+)/$', 'employer.views.verify'),
    url(r'^tracking/([0-9]+)/$', 'employer.views.applicant_tracking'),
    url(r'^shortlist/([0-9]+)/$', 'employer.views.shortlist'),
    url(r'^reject/([0-9]+)/$', 'employer.views.reject'),
    url(r'^admin/', include(admin.site.urls)),
)

urlpatterns += common.urls.urlpatterns

if  employer.settings.DEBUG:
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns
    #urlpatterns += patterns('',
                        #(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
                        #)
    
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': employer.settings.MEDIA_ROOT}))

else:
    pass
