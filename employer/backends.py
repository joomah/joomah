from common.models import Employer, Employer_auth

class EmployerAuthBackend:
    def authenticate(self, username=None, password=None, **kwargs):
        try:
            ja = Employer_auth.objects.get(auth_code=username)
            return ja.user
        except:
            return None

    def get_user(self, user_id):
        try:
            return Employer.objects.get(pk=user_id)
        except Employer.DoesNotExist:
            return None
