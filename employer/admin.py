from django.contrib import admin
from common.models import JobListing


class JobListingAdmin(admin.ModelAdmin):
    list_display = ('id','title', 'owner', 'num_applicants', 'end_date',)
admin.site.register(JobListing, JobListingAdmin)
