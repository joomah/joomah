import requests
from faker import Factory
from random import randrange

fake=Factory.create()

def rand(n):
	q=randrange(1,n+1)
	if q<10:
		q='0'+str(q)
	return str(q)

def createUser():
	m=requests.Session()
	q=m.get("http://localhost:8000/")
	csrf= m.cookies['csrftoken']
	password=fake.password()
	first_name = fake.first_name()
	last_name = fake.last_name()
	payload =  dict(csrfmiddlewaretoken=csrf, first_name=first_name, last_name=last_name, username=fake.email(), password1=password, password2=password)
	r = m.post("http://localhost:8000/signup/", data=payload, headers=dict(Referer="http://localhost:8000/"))
	csrf= m.cookies['csrftoken']
	birthday = rand(30)+ '-' + rand(12) + '-' + str(randrange(1900,1990))
	gender = ['M','F'].pop(randrange(0,2))
	degree = ['olevel','alevel','bachelors','masters','doctorate','professional'].pop(randrange(0,6))
	industry = ["Agriculture", "Accounting", "Advertising", "Aerospace", "Aircraft", "Airline", "Apparel & Accessories", "Automotive", "Banking", "Broadcasting", "Brokerage", "Biotechnology", "Call Centers", "Cargo Handling", "Chemical", "Computer", "Consulting", "Consumer Products", "Cosmetics", "Defense", "Department Stores", "Education", "Electronics", "Energy", "Entertainment & Leisure", "Executive Search", "Financial Services", "Food, Beverage & Tobacco", "Grocery", "Health Care", "Internet Publishing", "Investment Banking", "Legal", "Manufacturing", "Motion Picture & Video", "Music", "Newspaper Publishers", "Online Auctions", "Other", "Pension Funds", "Pharmaceuticals", "Private Equity", "Publishing", "Real Estate", "Retail & Wholesale", "Securities & Commodity Exchanges", "Service", "Soap and Detergent", "Software", "Sports", "Technology", "Telecommunications", "Television", "Transportaion", "Venture Capital"].pop(randrange(0,55))
	schoolstart = randrange(1900,1990)
	country = ['Kenya','Ghana'].pop(randrange(0,2))
	payload2  =  dict(csrfmiddlewaretoken=csrf, name=first_name + ' ' + last_name, date_of_birth=birthday, gender=gender, city=fake.city(), country=country, industry=industry, language='English', school1=fake.company(), start1=schoolstart, end1=schoolstart+4, degree1=degree, company1=fake.company(), title1="Manager",location1=fake.city(),description1="", study1="", startm1=1,starty1=1950,endm1=1,endy1=1960)
	d = m.post("http://localhost:8000/editprofile/", data=payload2, headers=dict(Referer="http://localhost:8000/signup/"))
	print "User " + first_name + " " + last_name + " created"

def createCompany():
	m=requests.Session()
	q=m.get("http://localhost:8000/empstartsignup/")
	csrf= m.cookies['csrftoken']
	industry = ["Agriculture", "Accounting", "Advertising", "Aerospace", "Aircraft", "Airline", "Apparel & Accessories", "Automotive", "Banking", "Broadcasting", "Brokerage", "Biotechnology", "Call Centers", "Cargo Handling", "Chemical", "Computer", "Consulting", "Consumer Products", "Cosmetics", "Defense", "Department Stores", "Education", "Electronics", "Energy", "Entertainment & Leisure", "Executive Search", "Financial Services", "Food, Beverage & Tobacco", "Grocery", "Health Care", "Internet Publishing", "Investment Banking", "Legal", "Manufacturing", "Motion Picture & Video", "Music", "Newspaper Publishers", "Online Auctions", "Other", "Pension Funds", "Pharmaceuticals", "Private Equity", "Publishing", "Real Estate", "Retail & Wholesale", "Securities & Commodity Exchanges", "Service", "Soap and Detergent", "Software", "Sports", "Technology", "Telecommunications", "Television", "Transportaion", "Venture Capital"].pop(randrange(0,55))

	password=fake.password()
	company = fake.company()
	payload =  dict(csrfmiddlewaretoken=csrf, first_name=company, username=fake.email(), year_founded= str(randrange(1900,2014)), password1=password, password2=password, industries = industry, taxID=str(randrange(0,1000000000)), address=fake.address(), last_name="company")
	r = m.post("http://localhost:8000/empsignup/", data=payload, headers=dict(Referer="http://localhost:8000/empstartsignup/"))
	csrf= m.cookies['csrftoken']


	payload2  =  dict(csrfmiddlewaretoken=csrf, employees = str(randrange(0,100000)), name = company, description=fake.bs())
	d = m.post("http://localhost:8000/empsave/", data=payload2, headers=dict(Referer="http://localhost:8000/empsignup/"))
	print "Company " + company + " created"

def createJob():
	x=1#not done

def main(users, companies, jobs):
	for i in range(0,users):
		createUser()
	for i in range(0,companies):
		createCompany()
	for i in range(0,jobs):
		createJob()
main(0,300,0)