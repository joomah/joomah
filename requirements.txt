Django>=1.6.5
Pillow>=2.4.0
python-magic>=0.4.6
#git+git://github.com/pinax/django-mailer.git#egg=mailer # since this annoying always redownloads
pytz
django-twilio
pisa
reportlab==2.7
html5lib
MySQL-python
stripe
