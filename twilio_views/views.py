from django.core.exceptions import ObjectDoesNotExist
from django_twilio.decorators import twilio_view
from twilio.twiml import Response
from common.models import Jobseeker, JobListing
import re

@twilio_view
def sms(request):
    if re.search("notice", request.POST.get('Body','notice'), re.IGNORECASE):
        return get_notices(request)
    return apply_for_job(request)


def apply_for_job(request):
    number = re.search("([0-9]+)",request.POST.get('Body', ''))
    r = Response()
    if number:
        try:
            number = int(number)
            job = JobListing.objects.get(id=number)
            applicant = Jobseeker.objects.filter(phone_number=request.POST['From'])[0]
            Application.objects.create(applicant=applicant, job=job).save()
            r.message("You have successfully applied to be a " + job.title + " at " + str(job.owner))
            return r
        except:
            pass
    r.message("That is not a valid Job Id or your phone number is not associated with an account.")
    return r

def get_notices(request):
    r = Response()
    try:
        user = Jobseeker.objects.get(phone_number=request.POST['From'])
        msg = "Reply with the number of the job to apply:"
        notices = user.notices()[:10]
        html_message = ''
        if len(notices) > 0:
            for job in notices:
                msg += "\n" + str(job.id) + ":" + unicode(job.owner) + " hiring " + job.title
                if job.body: msg += ", " + job.body[:20]
                if job.end_date: html_message += ", deadline: " + str(job.end_date)
                r.message(msg)
        else:
            r.message("There are no notices, please try again later.")
    except ObjectDoesNotExist:
        r.message("There is no account associated with phone number " + request.POST.get('From'))
    except:
        r.message("This request was not properly formatted.")
    return r
