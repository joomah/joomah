from django.core.management.base import BaseCommand
from twilio.rest import TwilioRestClient
from common.models import Jobseeker
import jobseeker.settings

class Command(BaseCommand):
    def handle(self, *args, **options):
        client = TwilioRestClient(jobseeker.settings.TWILIO_ACCOUNT_SID, jobseeker.settings.TWILIO_AUTH_TOKEN)
        jobseekers = Jobseeker.objects.exclude(phone_number__isnull=True)
        for jobseeker in jobseekers:
            msg = "Reply with the number of the job to apply:"
            for job in jobseeker.notices()[:10]:
                msg += "\n" + str(job.id) + ":" + job.owner + " hiring " + job.title
                if job.description: msg += ", " + job.description[:20]
                if job.deadline: msg += ", deadline: " + str(job.deadline)
            client.messages.create(to=jobseeker.phone_number, from_="+17323720828", body=msg)
        self.stdout.write("Successfully sent notice texts.")
